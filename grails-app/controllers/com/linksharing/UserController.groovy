package com.linksharing


import com.linksharing.resources.ReadingItem
import com.linksharing.resources.Resource
import grails.converters.JSON


import javax.sql.rowset.serial.SerialBlob
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = true)
class UserController {


    def userService

    def index() {
        if (session.user) {
            Visiblity[] visiblities = Visiblity.values()
            Seriousness[] seriousnesses = Seriousness.values()
            User user = session.user
            List<Resource> resourceList = user.resourceList()
            List<Subscription> subscriptionList = user.getSubscribedTopic(session.user)
            //Integer subcount=user.subscriptions.count()

            def results=userService.getPostCount(user)
            def result=userService.getSubscriptionSize(user)
            Integer subcount=result[0];
            Integer postCount=results[0];
            render(view: "/user/indexx", model: ['visiblities': visiblities,'user':user ,'resourceList': resourceList, 'topicVOList': userService.serviceMethod(), 'user': user, 'subcount':subcount,'seriousnesses': seriousnesses, 'subscriptionList': subscriptionList,'postCount':postCount])
        } else {
        }
    }


    def subscriptionList()
    {

        User user=session.user
        List<Subscription> subscriptionList = user.getSubscribedTopic(session.user)

    }


    def resourceList() {
        User user = User.findById(1)
        List<Resource> resourceList = Resource.createCriteria().list(max: 5) {

            createAlias('readingItems', 'ri')
            eq('ri.user', user)
            eq('ri.isRead', false)
        }

        render resourceList[0].createdBy

    }

/*gorm2 functionality4*/

    @Transactional
    def update(Long id) {
        if (ReadingItem.executeUpdate("update ReadingItem set isRead= :isRead" + " where id= :id", [isRead: true, id: id])) {
            render "success"
        } else {
            render "not update"
        }
    }


    def Updatee() {
        def uploadedFile = params.payload
        byte[] myByte = uploadedFile.getBytes()
        java.sql.Blob blob = null;
        blob = new SerialBlob(myByte)
        if (session.user) {
            User.executeUpdate("update User set firstName=?,lastName=?,email=?,userName=? ,photo=? where id=? ", [params.fName, params.lName, params.email, params.userName, blob,session.user.id])
            flash.message="profile update"

        } else {
            flash.message = "error in update"

        }
        redirect(controller: "user",action: "userProfile")

    }


    def show(User userInstance) {
        respond userInstance
    }


    def showPayload() {

        def id = Long.parseLong(params.id)
// write the image to the output stream
        User user = User.findById(id)
        Integer y = 1;
        long x = y.longValue();
        if (user.photo) {
            byte[] image = user.photo.getBytes(x, (int) user.photo.length())
            response.outputStream << image
            response.contentType = "image/png"
            response.outputStream.flush()
        } else {
            Path path = Paths.get('/home/arun/linksharing/web-app/images/images.jpeg')
            byte[] image = Files.readAllBytes(path);
            response.outputStream << image
            response.contentType = "image/png"
            response.outputStream.flush()
        }


    }

    def showUser(String userName) {

        if (session.user?.userName == userName) {
            render true
        } else {
            User user = User.findByUserName(userName)
            render user ? false : true
        }

    }

    def showUserr(String userName) {

        User user = User.findByUserName(userName)
        render user ? true : false


    }

    def showEmail(String email) {
        if (session.user?.email == email) {
            render true
        } else {
            User user = User.findByEmail(email)
            render user ? false : true
        }
    }


    def forgetPassword() {
        Random random = new Random()
        String email = params.email
        if (User.findByEmail(email)) {
            String sub = "regarding forget password"
            int num = 10
            Integer number=random.nextInt(10**num)

            String emailBody = """ you getting number to vaidate password

                                ${number}"""
            Password password = new Password(email: email, number: number)
            password.save(flush: true, failOnError: true)
            userService.sendTestMail(email, sub, emailBody)
            flash.message="email send for change password"
        } else {
            flash.message = "your email id doesn't exit"
            redirect(controller: 'login', action: 'index')
        }

    }

    def changePassword() {
        Integer number = Integer.parseInt(params.Number)
        String email = params.email
        String changePassword = params.changePassword
        Password password = Password.findByEmailAndNumber(email, number)
        if (password) {
            User user = User.findByEmail(email)
            user.executeUpdate("UPDATE User set password=? where email=?", [changePassword, email])
            flash.message = "change password done successfully"

        } else {
            flash.message = "change password not done successfully"

        }
        forward(controller: 'login', action: 'index')

    }


    def userData()
    {
        List<User> userList=User.createCriteria().list (max:20){}
        println "*********userData${userList}"
        def count=User.count()
        render(view: "/user/showadmindata", model: ['userList':userList,'count':count])
    }
    def userData1()
    {
        List<User> userList1=User.createCriteria().list (max:20){
            order("${params.name}","desc")
            }
        println "*********userData1${userList1}"
        def count=User.count()
        render(template: "/user/showadmindata", model: ['userList':userList1,'count':count])
    }

    def displaydatapage()
    {
        List<User> userList=User.createCriteria().list (max:20,offset:params.offset){}
        def count=User.count()
        render(view: "/user/showadmindata", model: ['userList':userList,'count':count])
    }

    def updateactivefield()
    {
        long id=Long.parseLong(params.id)
        User user=User.findById(id)
        user.activeField=true
        user.save(flush: true)
        List<User> userList=User.createCriteria().list (max:20){}
        def count=User.count()

        render(template: "/user/showadmindata", model: ['userList':userList,'count':count])


    }
    def updateactivefield1()
    {

        long id=Long.parseLong(params.id)
        User user=User.findById(id)
        user.activeField=false
        user.save(flush: true)
        List<User> userList=User.createCriteria().list (max:10){}
        def count=User.count()


        render(template: "/user/showadmindata", model: ['userList':userList,'count':count])

    }

    def userProfile()
    {
        User user = session.user
        def result=userService.getSubscriptionSize(user)
        Integer subcount=result[0];
        Topic topic= new Topic()
        List<Topic> topicList=topic.gettopics(session.user)
        Seriousness[] seriousnesses=Seriousness.values()
        Visiblity[] visiblities=Visiblity.values()
        render(view: '/user/_editProfile', model: ['topicList': topicList, 'seriousnesses':seriousnesses,'visiblities':visiblities,'user': user, 'subcount':subcount])
    }

    def response404()
    {
        println"under resonse"

        response.status = 404
        render([error: 'an error occurred'] as JSON)
    }

    def profile(ResourceSearchCO resourceSearchCO) {
        def searchString = params.userId
        params.max = Math.min(params.max ? params.int('max') : 1, 100)
        params.offset = (params.offset ? params.int('offset') : 0)
        User user = User.get(searchString) //resourceSearchCO.user
        List<Topic> topicList = user?.getSubscribedTopic(params)
        List<Topic> userTopics = Topic.findAllByCreatedBy(user, [max: 5])
        if ((session.user == user) || (session.user.admin)) {
            render view: '/user/profile', model: [
                    user           : resourceSearchCO.user,
                    subtopics      : topicList,
                    subtopicscount : topicList.size(),
                    usertopics     : userTopics,
                    usertopicscount: userTopics.size()
            ]
        } else {
            topicList = topicList.findAll { it.visiblity == Visiblity.PUBLIC}
            userTopics = userTopics.findAll { it.visiblity== Visiblity.PUBLIC}
            render view: '/user/profile', model: [user           : resourceSearchCO.user,
                                                  subtopics      : topicList, subtopicscount: topicList.size(),
                                                  usertopics     : userTopics,
                                                  usertopicscount: userTopics.size()]
        }
    }





    def search1()
    {
        List<Resource> resources=[]

        String sk="Grails"
        println "<<<${sk}"

        List<Resource> resourceList = Resource.createCriteria().list(){
            println"<<<<<<<<<<<"

            createAlias('topic','top')
            like("top.name", "%${sk}%")


        }
        List<Resource> resourceList1= Resource.createCriteria().list(){
            println"<<<<<<<<<<<"

            like("description","%${sk}%")


        }
        resources=resourceList + resourceList1

        render"6"
        render resourceList
        render resourceList1
        render resources


    }



}
