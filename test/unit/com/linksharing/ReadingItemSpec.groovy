package com.linksharing

import com.linksharing.resources.LinkResource
import com.linksharing.resources.ReadingItem
import com.linksharing.resources.Resource
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ReadingItem)
class ReadingItemSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test for reading item"() {

        given:
        ReadingItem readingItem=new ReadingItem(resource: resource,user: user, isRead: read)

        expect:
        readingItem.validate()==result

        where:
        resource           |user       |read |result
        new LinkResource() |new User() |true |true

    }
}