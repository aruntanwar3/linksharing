<%@ page import="com.linksharing.*" %>


<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="container" id="showdata">

    <div class="col-xs-6">
        <!-- Topic Panel -->
        <!-- ************************************************** -->
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Topic: "${topic.name}"</strong></div>

            <div class="panel-body">

                    <div class="col-xs-2">

                        <a href="<g:createLink controller="topic" action="index" params="[id:topic.createdBy.id]"></g:createLink>"> <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${topic.createdBy.id}" alt="Smiley face"
                                 style="border:1px solid black" height="70px" width="70px"></a>

                    </div>

                    <div class="col-xs-10">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="<g:createLink controller="topic" action="show"  params="[id:topic.id]" ></g:createLink>" class="text-left">${topic.name}</a>
                                <small class="text-muted">(${topic.visiblity})</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-3">
                                <small class="text-muted">@${topic.createdBy.firstName}</small>
                            </div>

                            <div class="col-xs-1"></div>

                            <div class="col-xs-3"><small class="text-muted">Subscription</small></div>

                            <div class="col-xs-1"></div>

                            <div class="col-xs-3"><small class="text-muted">Posts</small></div>

                            <div class="col-xs-1"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-3"><g:if test="${session.user}"><ls:checkSubscription topicid="${topic.id}"/></g:if></div>

                            <div class="col-xs-1"></div>

                            <div class="col-xs-3"><p class="text-info">${topic.subscriptions.size()}</p></div>

                            <div class="col-xs-1"></div>

                            <div class="col-xs-3"><p class="text-info">${topic.resources.size()}</p></div>

                            <div class="col-xs-1"></div>
                        </div>
<g:if test="${session.user}">
                        <div class="row">
                            <div class="col-xs-7">
                            </div>

                            <div class="col-xs-4">
                                %{--<div class="dropdown">
                                    <g:select name="seriousnesss" from="${seriousnesses}" value="it"></g:select>

                                </div>--}%
                            </div>

                            <div><span class="glyphicon glyphicon-envelope" onclick="sendinvite(${topic.id})"></span></div>
                        </div>
</g:if>
                    </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><strong>Users:${topic.name}</strong></div>
            <g:each in="${subscribedUser}">
                <div class="panel-body">
                    <div>
                        <div class="col-xs-2">
                            <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.id}" alt="Smiley face"
                                 style="border:1px solid black" height="70px" width="70px">

                        </div>

                        <div class="col-xs-10">
                            <div class="row">
                                <div class="col-xs-12">
                                    <span class="h3"><strong>${it.userName}</strong></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3">
                                    <small class="text-muted">@${it.firstName}</small>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3"><small class="text-muted">Subscription</small></div>

                                <div class="col-xs-1"></div>

                                <div class="col-xs-3"><small class="text-muted">Posts</small></div>

                                <div class="col-xs-1"></div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3"><p class="text-info">${it.subscriptions.size()}</p></div>

                                <div class="col-xs-1"></div>

                                <div class="col-xs-3"><p class="text-info">${it.resources.size()}</p></div>

                                <div class="col-xs-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
    </div>

    <div class="col-xs-6">
        <!-- Topic Posts Panel -->
        <!-- ************************************************ -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <strong>Posts: ${topic.name}</strong></div>

                    <div class="col-xs-4 pull-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term"
                                   id="srch-term">

                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="searchtopicnamedata(${topic.id})"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <g:each in="${resourceList}">
                <div class="panel-body">
                    <div>
                        <div class="col-xs-2">
                            <a href="<g:createLink controller="topic" action="index" params="[id:it.createdBy.id]"></g:createLink>">  <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.createdBy.id}" alt="Smiley face"
                                                                                                                                           style="border:1px solid black" height="80px" width="80px"></a>

                        </div>

                        <div class="col-xs-10">
                            <div class="col-xs-12">
                            <div id="topic-${it.id}" style="display: none;">
                                <span class="col-xs-3">
                                    <input type="text" id="topicn-${it.id}"   class="topicName" size="4" placeholder="${it.description}"/>
                                </span>
                                <span class="col-xs-3"><g:submitButton name="save" value="save" onclick='resourcenamechange(${it.id})'/></span>
                                <span class="col-xs-3"><g:submitButton name="cancel" value="cancel" onclick='resourcenamechange1(${it.id})'/></span>
                            </div>
                            </div>

                            <p>${it.description}</p>

                            <div class="row">
                                <div class="col-xs-1 fa fa-facebook-official"></div>

                                <div class="col-xs-1 fa fa-twitter"></div>

                                <div class="col-xs-1 fa fa-google-plus"></div>

                                <g:if
                                        test="${it instanceof com.linksharing.resources.DocumentResource}"><span><a
                                        href="<g:createLink controller="documentResource" action="download"
                                                            params="[id: it.id]"></g:createLink>"
                                        style="font-size:10px;">Download</a></span>
                                </g:if>
                                    <g:if test="${it instanceof com.linksharing.resources.LinkResource}">
                                        <span><ls:canbyviewlink topicid="${it.topic.id}" resourceid="${it}"/>
                                    </span>
                                    </g:if>

                                    <span><a href="<g:createLink controller="resource" action="showresource"
                                                                 params="[id: it.id]"></g:createLink>"
                                             style="font-size:10px;">view post</a></span>

                                <g:if test="${session.user}">
                                <g:if test="${it.createdBy.id== session.user?.id}">
                                    <span class="glyphicon glyphicon-file "onclick="updateingresource(${it.id})"></span>
                                </g:if>
                                </g:if>


                            </div>
                        </div>
                    </div>
                </div>
            </g:each>

        </div>
    </div>

</div>
</body>
</html>