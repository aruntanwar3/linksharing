package com.linksharing.resources

import com.linksharing.Subscription
import com.linksharing.Topic
import com.linksharing.User
import com.linksharing.resources.Resource

class DocumentResource extends Resource {

    String filepath
    Date dateCreated
    Date lastUpdated



    static DOCUMENT_CONTENT_TYPE="application/pdf"
    static transients = ['contenttype','fileName']
    static constraints = {

        /*contenttype validator: {

            this.contenttype=="application/pdf"?true:'contenttype.not.contain.pdf'
        }*/

    }



    String getFileName()
    {
        String[] split=this.filepath.split('/')
        return split[split.length-1]

    }

    def afterInsert= {

        log.info "insert after"
        withNewSession {

            Topic topic=this.topic
            List<Subscription> subscriptions=Subscription.findAllByTopic(topic)
            subscriptions.each {

                ReadingItem readingItem=new ReadingItem(isRead: false ,user: it.user,resource: this)
                readingItem.save(flush: true,failOnError: true)







            }


        }

    }

    }

