package com.linksharing

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.IgnoreRest
import spock.lang.Specification
import grails.test.GrailsUnitTestCase

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Subscription)
@Mock([User,Topic])
class SubscriptionSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test for subsciption"() {

        given:
        Subscription subscription=new Subscription(seriousness:serious, user: user, topic: tops)

        expect:
        subscription.validate()==result

        where:
        serious|user|tops|result
        Seriousness.CASUAL|new User()|new Topic()|true

    }


    void "checking uniqueness"()
    {
        given:
        User user=new User(email: 'normal@tothenew.com', password: "normal1234", firstName: "normal", admin: false, userName: "aruntanwar", lastName: "user", confirmPassword: "normal1234")
        Topic topic=new Topic(name: "topic", createdBy: user, visiblity: Visiblity.PUBLIC)

        when:
        user.save(flush: true)
        topic.save(flush: true)
        System.err.println(user.id)
        System.err.println(topic.id)
        Subscription subscription=new Subscription(seriousness: Seriousness.SERIOUS,user:user,topic: topic)
        subscription.save(flush: true)
        System.err.println("subscription saved : " + subscription.id)
        System.err.println("not save first time")

        then:


        subscription.count()==1
        System.err.println("not getting result first time")

        when:
        Subscription subscription1=new Subscription(seriousness: Seriousness.SERIOUS,user:user,topic: topic)

        System.err.println("create object with same user and same topic")


        then:
        boolean  isValid=subscription1.validate();

        System.err.println("subscription1.validate() >> " + isValid)
        if(!subscription1.save(flush: true))
        {
            subscription1.errors.each {
                System.err.println("Failing due to : " + it)
            }
        }
        else {
            System.err.println("Saved again with id : " + subscription1.id)
        }

        isValid == false

        System.err.println("not validate")


    }
}
