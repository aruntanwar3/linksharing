package com.linksharing

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.web.ControllerUnitTestMixin

//import grails.test.mixin.*
//import grails.test.mixin.web.ControllerUnitTestMixin
import spock.lang.*

@TestMixin([ControllerUnitTestMixin])
@TestFor(TopicController)
@Mock([Topic,User])
class TopicControllerSpec extends Specification {

    void "canary"(){

        when:
        1==1

        then:
        assert true
    }

    void "Topic is getting saved"() {

        given:
        System.err.println("under topic getting saved")
        System.err.println("under given")
        User user = new User(email: 'normal@tothenew.com', password: "normal1234", firstName: "normal", admin: false, userName: "aruntanwar", lastName: "user", confirmPassword: "normal1234")
        controller.session.user = user;
        Topic topic = new Topic(topicName: "topic12", createdBy: session.user, visibility: Visiblity.PUBLIC)

        when:
        System.err.println("under when")
        controller.save(topic.name, "PUBLIC")

        then:
        System.err.println("under then")
        flash.message == "topic.saved.message"
    }
}
