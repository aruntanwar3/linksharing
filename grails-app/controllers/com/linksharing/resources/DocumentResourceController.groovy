package com.linksharing.resources

import com.linksharing.Constants
import com.linksharing.Topic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


class DocumentResourceController {


    @Transactional
    def upload() {
        def file = params.document
        if (file.empty) {
            flash.message = "File cannot be empty"
        } else {
            Random random = new Random()
            Integer num = random.nextInt()
            def documentInstance = new DocumentResource()
            def filenames = num + file.originalFilename
            documentInstance.filepath = grailsApplication.config.linksharing.documents.folderPath + filenames
            documentInstance.createdBy = session.user
            Long id = Long.valueOf(params.topic).longValue();
            Topic topicn = Topic.findById(id)
            documentInstance.topic = topicn
            documentInstance.description = params.description
            file.transferTo(new File(grailsApplication.config.linksharing.documents.folderPath + filenames))
            if (documentInstance.save(flush: true, failOnError: true)) {
                flash.message = "document stored successfully"
            }
            else {
                flash.message = "document cannot stored successfully"
            }
        }

        redirect(controller: 'user', action: 'index')

    }


    def download(Long id) {
        println "under download"
        DocumentResource documentResource = DocumentResource.get(id)
        println("${documentResource}")
        if (documentResource == null) {
            println "not found file"
            flash.message = "Document not found."
            redirect(controller: 'user', action: 'index')

        } else {
            response.setContentType("${Constants.DOCUMENT_CONTENT_TYPE}")
            response.setHeader("Content-Disposition", "Attachment;Filename=\"${documentResource.fileName}\"")
            def file = new File(documentResource.filepath)
            def fileInputStream = new FileInputStream(file)
            def outputStream = response.getOutputStream()
            byte[] buffer = new byte[4096];
            int len;
            while ((len = fileInputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush()
            outputStream.close()
            fileInputStream.close()
        }
    }


}
