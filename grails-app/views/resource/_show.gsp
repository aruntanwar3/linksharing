<%@ page import="com.linksharing.*" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="container">

    <div class=" col-xs-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <div>
                    <div class="col-xs-2">
                        <div class="glyphicon glyphicon-user" style="font-size:80px"></div>
                    </div>

                    <div class="col-xs-10">
                        <div class="row" style="padding-bottom:5px">
                            <div class="col-xs-6">
                                <span class="h5">Arun Tanwar</span>
                            </div>

                            <div class="col-xs-3">
                            </div>

                            <div class="col-xs-3">
                                <a href="#" class="text-left">Grails</a>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom:10px">
                            <div class="col-xs-4">
                                <small class="text-muted">@Arun Tanwar</small>
                            </div>

                            <div class="col-xs-2"></div>

                            <div class="col-xs-6 text-muted">9:21 AM 10 February 2016</div>
                        </div>

                        <div class="row" style="padding-bottom:15px">
                            <div class="col-xs-8"></div>

                            <div class="col-xs-4">
                                <select class="form-control" id="sel1">
                                    <option>1 Star</option>
                                    <option>2 Star</option>
                                    <option>3 Star</option>
                                    <option>4 Star</option>
                                    <option>5 Star</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <p style="padding-bottom:5px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.</p>

                    <div class="row">
                        <div class="col-xs-1 fa fa-facebook-official" style="font-size:20px"></div>

                        <div class="col-xs-1 fa fa-twitter" style="font-size:20px"></div>

                        <div class="col-xs-1 fa fa-google-plus" style="font-size:20px"></div>

                        <div class="col-xs-6"></div>

                        <div class="col-xs-3"><a href="#">View Post</a></div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="col-xs-5" style="margin:15px">
        <ls:trendingtopic/>
    </div>
</div>
</body>
</html>
