package com.linksharing

import com.linksharing.resources.Resource

class Topic {
    String name
    User createdBy;
    Visiblity visiblity
    Date dateCreated
    Date lastUpdated
    static hasMany = [subscriptions: Subscription, resources: Resource]
    static belongsTo = [createdBy: User]
    static transients = ['TrendingTopic']

    static constraints = {
        visiblity nullable: false
        createdBy nullable: false
        name nullable: false, blank: false, unique: ['createdBy']


    }

    static mapping = {
          sort name: 'asc'
        topics fetch: 'join'
    }

    def afterInsert={

        log.info "insert after"
        withNewSession {
            println "under insert"
            Subscription subscription = new Subscription(seriousness: Seriousness.SERIOUS);
            this.addToSubscriptions(subscription)
            createdBy.addToSubscriptions(subscription)
            println "subscription after topic created"
            if (!subscription.save(flush: true)) {
                log.error("subscription not save automatically")
            }
        }

    }

    static List<TopicVO> getTrendingTopic() {
        List trendingTopic = Resource.createCriteria().list(max: 5) {
            createAlias("topic", "topicAlias")
            projections {
                groupProperty('topic')
                count('createdBy', 'mycount')
                property('topicAlias.id')
                property('topicAlias.name')
                property('topicAlias.visiblity')
                property('topicAlias.createdBy')
            }
            order('mycount', 'desc')
            order('topicAlias.name', 'desc')
            eq('topicAlias.visiblity', Visiblity.PUBLIC)

        }

        List<TopicVO> trendingTopics = []
        trendingTopic.eachWithIndex { val, ind ->
            trendingTopics.add(new TopicVO(id: val.getAt(2), name: val.getAt(3), count: val.getAt(1), visiblity: val.getAt(4), createdBy: val.getAt(5),topic: val.getAt(0)))
        }
        return trendingTopics

    }

    List<Topic> gettopics(User user)
    {
        List<Topic> topicList=Topic.createCriteria().list(max:5)
                {
                    eq('createdBy',user)
                    eq('visiblity', Visiblity.PUBLIC)
                }
        return topicList

    }
    List<Topic> gettopic(User user)
    {
        List<Topic> topicListt=Topic.createCriteria().list(max:5)
                {
                    eq('createdBy',user)

                }

        return topicListt
    }


    static List<User> getSubscribedUsers()
    {
        List<User> subcription=User.createCriteria().list {
            createAlias('subscriptions','sub')
            projections {
                property('sub.user')
            }
        }
    }

    Boolean isPublic()
    {
        if(this.visiblity==Visiblity.PUBLIC)
        {
            return true
        }
        else
        {
            return false
        }

    }

    def canViewedBy(User user,Topic topic)
    {
        if(isPublic()==true && user.subscriptions.topic==topic &&(user.admin==true))
        {
        }

    }
}
