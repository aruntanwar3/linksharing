package com.linksharing



import grails.test.mixin.*
import spock.lang.*

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification



@TestFor(SubscriptionController)
@TestMixin(GrailsUnitTestMixin)
@Mock([Topic, User, Subscription])
class SubscriptionControllerSpec extends Specification {

@IgnoreRest
    void "test To test save"() {
        given:

        User user=new User(email: 'normal@tothenew.com', password: "normal1234", firstName: "normal", admin: false, userName: "aruntanwar", lastName: "user", confirmPassword: "normal1234")
        user.save()
        controller.session.user = user
        Topic topic = new Topic(topicName: "topic", createdBy: user, visibility:Visiblity.PUBLIC)

        topic.save()
        when:
        controller.save(topic.id)
        then:
        response.contentAsString == "sucess"
    }

    void "test for subscription delete"() {
        when:
        new Subscription(user: new User(), topic: new Topic(), seriousness: Seriousness.SERIOUS).save(flush: true)
        controller.delete(1)
        then:
        response.contentAsString == "Sucess in delete"

    }

    void "test for subscription update"() {
        when:
        new Subscription(user: new User(), topic: new Topic(), seriousness: Seriousness.CASUAL).save(flush: true)
        controller.update(1, Seriousness.SERIOUS)
        then:
        response.contentAsString == "Sucess"
    }




}
