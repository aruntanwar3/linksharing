package com.linksharing

import com.linksharing.resources.LinkResource
import com.linksharing.resources.Resource
import com.linksharing.resources.ResourceRating
import grails.transaction.Transactional
import org.hibernate.ObjectNotFoundException


class TopicController {


def userService

    def index() {
        long id=Long.parseLong(params.id)
        User user1=User.findById(id)
        Topic topic=new Topic()
        LinkResource linkResource=new LinkResource()
        List<Topic> topicList=topic.gettopic(user1)
        List<Topic> topics=topic.gettopics(user1)
        List<Resource> resourceList=linkResource.getResourceList(user1)
        Seriousness[] seriousnesses=Seriousness.values()

        render(view: '/user/_profile', model: ['topicList': topicList, 'topics': topics, 'resourceList': resourceList,'seriousnesses':seriousnesses,'user1':user1])
    }

    def show(Long id) {
        User user = new User()
        Topic topic = Topic.findById(id)
        List<User> subscribedUser = user.getSubscribedUser(topic)
        List<Resource> resourceList = Resource.findAllByTopic(topic)
        Seriousness[] seriousnesses=Seriousness.values()
        TopicVO topicVO = new TopicVO(id: id, count: Resource.countByTopic(topic),name: topic.name, createdBy: topic.createdBy, visiblity: topic.visiblity)
        render(view: '/topic/show', model: ['subscribedUser': subscribedUser, 'resourceList': resourceList, 'topicVO': topicVO, 'topic': topic,'seriousnesses':seriousnesses])

    }

    def showbypage(Long topicid,int pagenumber, int numberofperpage)
    {

        List<Topic> topics=Topic.createCriteria().list(max:pagenumber*numberofperpage) {

             eq('id',topicid)
        }

    }


    def delete(Long id) {
        Topic topic=Topic.findById(id)
        if (topic) {
            try {
                topic.delete(flush: true)
                flash.message="Topic deleted"
            } catch (ObjectNotFoundException e) {
                render "${e.message}"
            }

        } else {

        }
        render "success"

    }

    def update()
    {
        String id = params.id
        Visiblity visiblity=Visiblity.valueOf(params.visiblity)

        def topicId = Long.parseLong(id.trim())
        Topic topic=Topic.findById(topicId)
        Topic.executeUpdate("UPDATE Topic set visiblity=? where id=?",[visiblity,topicId])
        render "success"


    }


    def updatetopicname()
    {
        String id = params.id
        def topicId = Long.parseLong(id.trim())
        Topic topic=Topic.findById(topicId)
        Topic topic1=Topic.findByNameAndCreatedBy(params.topic,session.user)
        if(!topic1)
        {
            Topic.executeUpdate("UPDATE Topic set name=? where id=?",[params.topic,topicId])
            flash.message=message(code: 'Topic.name.change')
        }
        else
        {
            flash.message="Topic name already created by you"
        }
        render "success"








    }
     def email()
     {
         String sub="regarding topic invite"
         String emailBody="""Your cordially invite
                              for this topic
                              ${params.topic}

                                 """
         userService.sendTestMail(params.email,sub,emailBody)
         flash.message="Send Email to invite"
         redirect(controller: 'user', action: 'index')

     }



    def save(String topic, String seriousness) {
        Topic topics = new Topic(visiblity: Visiblity.valueOf(seriousness), name: topic, createdBy: session.user)
        if (topics.save(flush: true)) {
         flash.message="Topic successfully saved"
        } else {
            flash.error="Error in saving topic"
        }
        redirect(controller: 'user',action: 'index')
    }

    def topicsearch()
    {

        String sk=params.search
        long topicid=Long.parseLong(params.id)
        List<Resource> resourceList = Resource.createCriteria().list(){
            println"<<<<<<<<<<<"

            createAlias('topic','top')
            like("top.name", "%${sk}%")
            eq("top.id",topicid)


        }



    }


    def toppost() {
        List result = ResourceRating.createCriteria().list(max: 5) {
            createAlias('resource', 'res')
            projections {
                groupProperty('resource')
                avg('score')
                property('res.id')

            }



            order('score', 'desc')
        }
        List<Resource> resourceList = []
        result.eachWithIndex { val, inx ->
            Resource resource = Resource.get(val.getAt(2))
            resourceList.add(resource)
        }

        render resourceList[0].createdBy.userName


    }



}
