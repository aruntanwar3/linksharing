package com.linksharing.resources

import com.linksharing.User
import com.linksharing.resources.Resource

class ReadingItem {
    Resource resource
    User user
    boolean isRead
    Date dateCreated
    Date lastUpdated
    static belongsTo = [resource:Resource]

    static constraints = {
        resource nullable: true , unique: ["user"]
        user nullable: false
        isRead nullable:false
    }


    def after
}
