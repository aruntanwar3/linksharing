package com.linksharing.resources

import com.linksharing.User
import com.linksharing.resources.Resource

class ResourceRating {
//    Resource resource
    Integer score
    User user
    Date dateCreated
    Date lastUpdated

    static belongsTo = [resource:Resource]

    static constraints = {

        score  min: 1,max: 5
        user unique: ['resource']
    }
}
