package com.linksharing

import com.linksharing.resources.LinkResource
import com.linksharing.resources.Resource
import com.linksharing.resources.ResourceRating
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ResourceRating)

class ResourceRatingSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test for resource rating"() {

        given:
        ResourceRating resourceRating=new ResourceRating(resource: resource,score: score,user: user)

        expect:
        resourceRating.validate()==result

        where:
        resource           |score |user       |result
        new LinkResource() |2     |new User() |true


    }
}
