package com.linksharing.resources

import com.linksharing.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


class ResourceRatingController {

    def resourceRatingSave( Long resourceid, int score) {

        println "under resource controller+${resourceid}+${score}"
        User user=session.user
        Resource resource=Resource.get(resourceid)
        println "check resource+${resource}"
        println "check user+${user}"
        ResourceRating resourceRating1=ResourceRating.findByResourceAndUser(resource,user)
        if(resourceRating1)
        {
           ResourceRating.executeUpdate("update ResourceRating set score=? where resource=? and user=?",[score,resource,user])
            render "success"
            println "under update"
        }
        else {
            ResourceRating resourceRating = new ResourceRating(score: score, user: user)
            resource.addToResourceRating(resourceRating)
            if(resource.save(flush: true,failOnError: true))
            {

                render "success"
            }

        }
        redirect(controller: 'resource', action: 'showresource',params: [id:resourceid])



    }
}

