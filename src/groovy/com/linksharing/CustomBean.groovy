package com.linksharing


class CustomBean {
    //static scope = 'proptype'

    void setFirstName(String firstName) {
        this.firstName = firstName;
        println("hello in custom bean")
    }

    String getFirstName() {
        println("in gettter")
        return firstName
    }
    String firstName

    CustomBean(String firstName) {
        println("in constructor")
        this.firstName = firstName

    }

    CustomBean()
    {
        println("default")
    }

}
