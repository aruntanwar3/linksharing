<%@ page import="com.linksharing.Topic" %>
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>

            <h4 class="modal-title"><span>Send Invitation(popup)</span></h4>

        </div>

        <div class="modal-body">
            <g:form controller="topic" action="email">
                <label for="link" class="col-xs-3">Email*:</label>
                <input type="email"  name="email" class="form-control col-xs-7 " id="lik" placeholder="Name" style="width:400px">

                <br>
                <br>
                <label for="topic" class="col-xs-3">Topic*:</label>

                <div class="dropdown col-xs-7 " style="width:400px">

                    <g:select class="SelectVisiblity" name="topic" from="${com.linksharing.Topic.list()}" optionValue="name"
                              optionKey="id"></g:select>
                    %{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Topic
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                    </ul>--}%
                </div>
                <br>
                <br>

                <div>
                    <g:actionSubmit value="send" controller="topic" action="email"/><span></span>
                    <g:actionSubmit value="Cancel"/>
                </div>
            </g:form>
        </div>
    </div>
</div>