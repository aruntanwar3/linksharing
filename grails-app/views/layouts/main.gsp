<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <asset:javascript src="application.js"/>

    <script>

        $(document).ready(function () {
            $('a.subscribe').on('click', function (e) {

                console.log($(this).attr('id'));
                savesubscription($(this).attr('id'));

                setTimeout(function () {
                    window.location.reload()
                }, 200)
            });

            $('a.unsubscribe').on('click', function (e) {

                console.log($(this).attr('id'));
                unsubscribed($(this).attr('id'));
                setTimeout(function () {
                    window.location.reload()
                }, 200)
            });
        })
        function updateuser1(userid) {
            $.ajax({
                        url: "/user/updateactivefield",
                        data: {id: userid}
                    })
                    .done(function (data) {

                        $('#admindata').html(data);
                    })
                    .fail(function () {
                        console.log("failure")
                    })

        }
        function searchtopicnamedata(topicid) {
            var Search = $('#srch-term').val()
            $.ajax({
                        url: "/topic/topicsearch",
                        data: {id: topicid, search: Search}

                    })
                    .done(function (data) {
                        /*$('#admindata').html(data)*/


                    })
                    .fail(function () {
                        console.log("failure")
                    })


        }
        function changeOrder(name) {

            $.ajax({
                        url: "/user/userData1",
                        data: {name: name}

                    })
                    .done(function (data) {
                        $('#admindata').html(data)


                    })
                    .fail(function () {
                        console.log("failure")
                    })

        }

        function sendinvite(topicid)
        {
            $('#myModal2').modal('toggle');
            $('.SelectVisiblity option[value='+topicid+']').attr('selected','selected');


        }
        function updateuser(userid) {
            $.ajax({
                        url: "/user/updateactivefield1",
                        data: {id: userid}
                    })
                    .done(function (data) {
                        console.log(data);
                        $('#admindata').html(data);
                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })
                    .fail(function () {
                        console.log("failure")
                    })

        }

        function searchtopic() {
            var searchtop = $('.searchclass').val()
            $.ajax({
                        url: "/login/topicSearch",
                        data: {search: searchtop}
                    })
                    .done(function (data) {
                        console.log(data);
                        $('#showdata').html(data);
                    })
                    .fail(function () {
                        console.log("failure")
                    })

        }


        function updateingtopic(topicid) {


            $('#topic-' + topicid).show()

        }
        function topicnamechange1(topicid) {

            $('#topic-' + topicid).hide()

        }
        function updateingresource(resourceid) {


            $('#topic-' + resourceid).show()

        }


        function topicnamechange(topicid) {
            var topicnamee = $('#topicn-' + topicid).val()
            $.ajax({
                        url: "/topic/updatetopicname",
                        data: {topic: topicnamee, id: topicid}
                    })
                    .done(function () {
                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })
                    .fail(function () {
                        $('#showmessage').text("Topic name already created by youu")
                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })


        }
        function resourcenamechange1(resourceid) {

            $('#topic-' + resourceid).hide()

        }
        function resourcenamechange(resourceid) {
            var topicnamee = $('#topicn-' + resourceid).val()
            $.ajax({
                        url: "/resource/updateresourcename",
                        data: {resource: topicnamee, id: resourceid}
                    })
                    .done(function () {

                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })
                    .fail(function () {

                        setTimeout(function () {
                            window.location.reload()
                        }, 200)

                    })
        }

        function deletetopic(topicid) {
            $.ajax({
                        url: "/topic/delete",
                        data: {id: topicid}
                    })
                    .done(function () {
                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })
                    .fail(function () {
                        alert("error");
                        setTimeout(function () {
                            window.location.reload()
                        }, 200)
                    })

        }
        function changeSeriousness(topicid) {

            var seriousness = $('#seriousness-' + topicid).val()
            console.log("under change seriousness" + seriousness)
            $.ajax({
                        url: "/subscription/updateTo",
                        data: {id: topicid, seriousness: seriousness}
                    })
                    .done(function () {
                        $('#showmessage').text("Seriousness updated")
                        console.log("success");
                    })
                    .fail(function () {
                        console.log("error");
                    })

        }


        function changeVisiblity(topicid) {

            var visiblity = $('#visiblities').val()


            $.ajax({
                        url: "/topic/update",
                        data: {id: topicid, visiblity: visiblity}
                    })
                    .done(function () {
                        $('#showmessage').text("Topic visiblity updated")
                        console.log("sucess")
                    })
                    .fail(function () {
                        console.log("error");
                    })

        }

        function savedata(str, resource) {
            $.ajax({

                        url: "/resourceRating/resourceRatingSave",
                        data: {resourceid: resource, score: str}
                    })
                    .done(function () {
                        $('#showmessage').text("resource rating save")
                        console.log("sucess")
                    })
                    .fail(function () {
                        console.log("error")
                    })
        }

        function savesubscription(topicid) {


            console.log("under subscription")
            $.ajax({
                url: "${createLink(controller: 'subscription',action: 'save')}",
                data: {id: topicid}
            }).done(function () {

                $('#showmessage').text("user Subscribed")

                console.log("done");
            }).fail(function () {
                console.log("ajax failed");

            });

        }


        function unsubscribed(subid) {
            console.log(subid);
            console.log('under unsubscribe')
            $.ajax({
                url: "${createLink(controller: 'subscription',action: 'delete')}",
                data: {id: subid}
            }).done(function () {

                $('#showmessage').text("user UnSubscribed")

                console.log("done");
            }).fail(function () {
                console.log("ajax failed");
            });


        }


        $(function () {

            $(".form-horizontal").validate({
                rules: {
                    userName: {
                        required: true,
                        remote: "http://localhost:8080/user/showUserr"
                    }
                },
                messages: {
                    userName: {
                        required: "please enter your user name",
                        remote: "username not exits"
                    }
                },
            })


            // Setup form validation on the #register-form element
            $("#register-form").validate({

                // Specify the validation rules
                rules: {
                    fName: {


                        pattern: /^[A-Za-z\w]{4,20}/,
                        required: true,
                        blank: false
                    },
                    lName: {
                        required: true,
                        blank: false
                    },
                    email: {
                        required: true,
                        remote: "http://localhost:8080/user/showEmail",
                        email: true
                    },
                    pwd: {
                        required: true,
                        minlength: 5
                    },
                    userName: {
                        required: true,
                        remote: "http://localhost:8080/user/showUser"


                    },
                    passwd: {
                        required: true,
                        equalTo: "#newpwd"

                    },
                    payload: {
                        required: true
                    },

                },

                // Specify the validation error messages
                messages: {
                    fName: {
                        required:"please enter your name"

                    },
                    lName: {
                        required: "Please enter your last name",
                        blank: "your last name is not empty"
                    },
                    pwd: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: {
                        email: "Please enter a valid email address",
                        remote: "email id already exit"
                    },
                    passwd: {
                        required: "Please provide a confirm password",

                    },
                    payload: "please provide photo",
                    userName: {
                        required: "please enter your user name",
                        remote: "username already exits"
                    }

                },
                /*submitHandler: function(form) {
                 form.submit();
                 }*/
            });
            $(".register-form1").validate({

                // Specify the validation rules
                rules: {
                    fName: {
                        pattern:/^[A-Za-z\w]{4,20}/,
                        required: true,
                        blank: false
                    },
                    lName: {
                        pattern: "[a-z]+",
                        required: true,
                        blank: false
                    },
                    email: {
                        remote: "http://localhost:8080/user/showEmail",
                        required: true,
                        email: true
                    },

                    userName: {pattern: "[a-z0-9]+",
                        required: true,
                        remote: "http://localhost:8080/user/showUser"


                    },

                    payload: {
                        ContentType:  "image/png",
                        required: true
                    },

                },

                // Specify the validation error messages
                messages: {
                    fName: {
                        pattern:"Invalid format",
                        required: "Please enter your first name",
                        blank: "your first name is not empty"
                    },
                    lName: {
                        required: "Please enter your last name",
                        blank: "your last name is not empty"
                    },

                    email: {
                        email: "Please enter a valid email address",
                        remote: "email id already exit"

                    },
                    payload: {
                        required:"please provide photo",
                        ContentType:"please add only jpeg file"
                    },
                    userName: {
                        required: "please enter your user name",
                        remote: "username already exits"
                    }

                }
                /*submitHandler: function(form) {
                 form.submit();
                 }*/
            });


        });



    </script>


    <g:layoutTitle>LINKSHARING</g:layoutTitle>

</head>

<body>

<div class="navbar navbar-default ">
    <ul class="nav navbar-nav navbar-left">
        <div class="navbar-header">
            <a class="navbar-brand" href="<g:createLink controller="user" action="index"></g:createLink>"
               style="font-size:25px"><strong>Link Sharing</strong></a>

        </div>
    </ul>
    <g:if test="${session.user}">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class=" glyphicon glyphicon-user "></i>${session.user.firstName}
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<g:createLink controller="user" action="userProfile"></g:createLink>">profile</a></li>
                    <g:if test="${session.user.admin}">
                        <li><a href="<g:createLink controller="user" action="userData"></g:createLink>">UserDetail</a>
                        </li>
                    </g:if>
                    <li><a href="<g:createLink controller="login" action="logout"></g:createLink>">Logout</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">

            <li><a href="#myModal4" id="mybtn1" data-toggle="modal">
                <span class="glyphicon glyphicon-file pull-right" style="height:25px"></span></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">

            <li><a href="#myModal2" id="mybtn3" data-toggle="modal">
                <span class="glyphicon glyphicon-envelope pull-right" style="height:25px"></span></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#myModal1" id="mybtn" data-toggle="modal">

                <span class="glyphicon glyphicon-link pull-right" style="height:25px"></span></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#myModal3" id="mybtn2" data-toggle="modal">
                <span class="glyphicon glyphicon-comment pull-right" style="height:25px"></span></a>
            </li>
        </ul>

    </g:if>

    <form class="navbar-form navbar-right" role="search">
        <div class="form-group has-feedback has-feedback-left">
            <input type="text" class="form-control searchclass" id="searchclass" placeholder="Search"
                   onkeyup="searchtopic()"/>
            <i class="form-control-feedback glyphicon glyphicon-search "></i>
        </div>
    </form>
</div>
<g:if test="${flash.message}">
    <div class="alert-success">${flash.message}</div>

</g:if>
<g:if test="${flash.error}">
    <div class="alert-danger">${flash.error}</div>
</g:if>
<div class="alert-success" id="showmessage"></div>
<br>
<g:layoutBody/>
<div class="footer" role="contentinfo"></div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>

<div id="myModal3" class="modal fade" role="dialog">

    <g:render template="../topic/create"></g:render>
</div>

<div id="myModal2" class="modal fade" role="dialog">

    <g:render template="../topic/email"></g:render>
</div>

<div id="myModal1" class="modal fade" role="dialog">

    <g:render template="../linkResource/create"></g:render>
</div>

<div id="myModal4" class="modal fade" role="dialog">

    <g:render template="../documentResource/create"></g:render>
</div>
<div  id="myModal6"  class="modal fade"  role="dialog">
    <g:render template="../user/changePassword"></g:render>
</div>


</body>
</html>
