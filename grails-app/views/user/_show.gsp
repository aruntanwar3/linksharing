<%@ page trimDirectiveWhitespaces="true" %>
<div class="panel panel-default " style="border:1px solid black ">
<g:if test="${session.user && user}">
    <div class="panel panel-body" style="-webkit-box-shadow: none">
        <div class="col-xs-4">
            <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${user.id}" alt="Smiley face"
                 style="border:1px solid black" height="120px" width="150px">
        </div>


        <div class="col-xs-8">
            <span class="col-xs-12"><strong><h6>${user.userName}</h6></strong></span>
            <span class="col-xs-12">@${user.firstName}</span>

            <div class="col-xs-6">
                <span>Subscriptions</span><br/>
                <span>${subcount}</span>
            </div>

            <div class="col-xs-6">
                <span>Topics</span><br/>
                <span>${postCount}</span>
            </div>
        </div>
    </div>
</g:if>

    <g:if test="${!session.user}">
    <div class="panel panel-body" style="-webkit-box-shadow: none">
        <div class="col-xs-4">
            <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${user1?user1.id:session.user.id}" alt="Smiley face"
                 style="border:1px solid black" height="120px" width="150px">
        </div>


        <div class="col-xs-8">
            <span class="col-xs-12"><strong><h6>${user1?user1.userName:session.user.userName}</h6></strong></span>
            <span class="col-xs-12">@${user1?user1.firstName:session.user.firstName}</span>

            <div class="col-xs-6">
                <span>Subscriptions</span><br/>
                <span>${user1?user1.subscriptions.size():session.user.subscriptions.size()}</span>
            </div>

            <div class="col-xs-6">
                <span>Topics</span><br/>
                <span>${user1?user1.topics.size():session.user.topics.size()}</span>
            </div>
        </div>
    </div>
    </g:if>
</div>
