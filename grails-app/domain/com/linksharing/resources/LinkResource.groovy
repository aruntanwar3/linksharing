package com.linksharing.resources

import com.linksharing.Subscription
import com.linksharing.Topic
import com.linksharing.User


class LinkResource extends Resource {

    String url
    Date dateCreated
    Date lastUpdated
    static constraints = {
        url url: true
    }

   def afterInsert= {

        log.info "insert after"
        withNewSession {

            Topic topic=this.topic
            List<Subscription> subscriptions=Subscription.findAllByTopic(topic)
            subscriptions.each {
                ReadingItem readingItem=new ReadingItem(isRead: false ,user: it.user,resource: this)
                readingItem.save(flush: true,failOnError: true)




            }


        }

    }




}
