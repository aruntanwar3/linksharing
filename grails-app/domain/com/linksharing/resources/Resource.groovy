package com.linksharing.resources

import com.linksharing.RatingInfoVO
import com.linksharing.ResourceSearchCO
import com.linksharing.Seriousness
import com.linksharing.Subscription
import com.linksharing.Topic
import com.linksharing.TopicVO
import com.linksharing.User
import com.linksharing.Visiblity

abstract class Resource {
    User createdBy
    String description
    Topic topic
    Date dateCreated
    Date lastUpdated


    static transients = ['ratingInfoVO',]
    static hasMany = [resourceRating: ResourceRating, readingItems: ReadingItem,]
    static belongsTo = [topic: Topic]
    static constraints = {
        description type: 'text'
    }


    static namedQueries = {

        search {
            ResourceSearchCO co ->
                if (co.topicId) {

                    eq(topic.id, co.topicId)
                    eq(topic.visiblity, co.visiblity.PUBLIC)
                }
        }
    }


    List<Resource> getResourceList(User user)
    {
        List<Resource> resources=Resource.createCriteria().list(max:5)
                {
                    eq('createdBy',user)
                }
    }

    void deleteResource()
    {
        log.info("this will be implemented in linkresource")
    }



    RatingInfoVO getRatingInfoVO() {
        List result = ResourceRating.createCriteria().get {
            projections {
                count("id")
                sum("score")
                avg("score")

            }

        }
        RatingInfoVO ratingInfoVO = new RatingInfoVO(totalScore: result[1], totalVote: result[0], averageScore: result[2])
        return ratingInfoVO
    }
}
