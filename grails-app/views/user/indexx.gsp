<%--
  Created by IntelliJ IDEA.
  User: arun
  Date: 2/3/16
  Time: 3:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="container" id="showdata">
    <div class=" col-xs-5">
        <g:render template="/user/show" model="['user': user, 'subcount': subcount,'postCount':postCount]"/>

        <div class="panel panel-default " style="border:1px solid black " id="subscription_list">
            <div class=" panel panel-heading" style=" border:1px solid black;">
                <div class="row"><span class="col-xs-8">Subscription</span>
                    <a class="col-xs-4" href="<g:createLink controller="subscription" action="index"/> ">view all</a>
                </div>

            </div>

            <g:each in="${subscriptionList}">

                <div class="panel panel-body " style="-webkit-box-shadow: none">

                    <div class="col-xs-4">
                        <a href="<g:createLink controller="topic" action="index"
                                               params="[id: it.topic.createdBy.id]"></g:createLink>"><img
                                src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.topic.createdBy.id}"
                                alt="Smiley face"
                                style="border:1px solid black" height="80px" width="80px"></a>
                    </div>

                    <div class="col-xs-8">
                        <div id="topic-${it.topic.id}" style="display: none;">
                            <span class="col-xs-3">
                                <input type="text" id="topicn-${it.topic.id}" class="topicName" size="4"
                                       placeholder="${it.topic.name}"/>
                            </span>
                            <span class="col-xs-3"><g:submitButton name="save" value="save"
                                                                   onclick='topicnamechange(${it.topic.id})'/></span>
                            <span class="col-xs-3"><g:submitButton name="cancel" value="cancel"
                                                                   onclick='topicnamechange1(${it.topic.id})'/></span>

                        </div>

                        <span class="col-xs-12"><a href="<g:createLink controller="topic" action="show"
                                                                       params="[id: it.topic.id]"></g:createLink>">${it.topic.name}</a>
                        </span>
                        <span class="col-xs-5">@${it.topic.createdBy.firstName}</span><span
                            class="col-xs-5">subscribe</span><span
                            class="col-xs-2">Post</span><span class="col-xs-5">
                        <g:if test="${session.user && it.topic.createdBy.id != session.user.id}"><ls:checkSubscription
                                topicid="${it.topic.id}"/></g:if></span><span
                            class="col-xs-5"><ls:subscriptioncount topicid="${it.topic.id}"/></span><span
                            class="col-xs-2">${it.topic.resources.size()}</span>
                    </div>

                    <div class="col-xs-12" style="margin:0px; ">

                        <g:if test="${it.user = session.user}">

                            <div class="col-xs-4">

                                <div class="dropdown">
                                    <g:select id="seriousness-${it.id}" class="seriousnesses" name="seriousnesses"
                                              style="width: 120px"
                                              from="${com.linksharing.Seriousness.values()}"
                                              keys="${com.linksharing.Seriousness.values()*.name()}"
                                              value="${it.seriousness}" onchange="changeSeriousness(${it.id})"/>
                                    %{--<g:select name="seriousnesses" id="seriousnesses" from="${seriousnesses}" value="${it.seriousness}" onchange="changeSeriousness(${it.id})"></g:select>--}%

                                </div>
                            </div>
                        </g:if>





                        <g:if test="${session.user}">
                            <g:if test="${it.topic.createdBy.id == session.user.id}">
                                <div class="col-xs-4">
                                    <div class="dropdown">

                                        <g:select id="visiblities" class="visblity" name="visiblities"
                                                  from="${com.linksharing.Visiblity.values()}"
                                                  keys="${com.linksharing.Visiblity.values()*.name()}"
                                                  value="${it.topic.visiblity}"
                                                  onchange="changeVisiblity(${it.topic.id})"/>

                                    </div>

                                </div>

                                <div class="col-xs-4">
                                    <span class="glyphicon glyphicon-envelope" onclick="sendinvite(${it.topic.id})"></span>
                                    <span class="glyphicon glyphicon-file "
                                          onclick="updateingtopic(${it.topic.id})"></span>
                                    <span class="glyphicon glyphicon-trash "
                                          onclick="deletetopic(${it.topic.id})"></span></div>

                            </g:if>
                        </g:if>

                    </div>

                </div>
            </g:each>

        </div>


        <div class="panel panel-default " style="border:1px solid black ">
            <div class=" panel panel-heading" style=" border:1px solid black;">
                <div class="row"><span class="col-xs-8">Trending Topic</span>
                    <a class="col-xs-4" href="<g:createLink controller="linkResource" action="index"/> ">view all</a>

                </div>
            </div>
            <ls:trendingtopic/>

        </div>

    </div>

    <div class="col-xs-6" style="margin:15px">
        <div class="panel panel-default" style="border:1px solid black">
            <div class="panel panel-heading" style="margin:0px; border:1px solid black ">
                <div>Inbox</div>
            </div>

            <g:each in="${resourceList}">

                <div class="panel panel-body" style="-webkit-box-shadow: none">

                    <div class="col-xs-4">
                        <a href="<g:createLink controller="topic" action="index"
                                               params="[id: it.createdBy.id]"></g:createLink>"><img
                                src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.createdBy.id}"
                                alt="Smiley face"
                                style="border:1px solid black" height="80px" width="80px"></a>
                    </div>

                    <div class="col-xs-8">
                        <div id="topic-${it.id}" style="display: none;">
                            <span class="col-xs-3">
                                <input type="text" id="topicn-${it.id}" class="topicName" size="4"
                                       placeholder="${it.description}"/>
                            </span>
                            <span class="col-xs-3"><g:submitButton name="save" value="save"
                                                                   onclick='resourcenamechange(${it.id})'/></span>
                            <span class="col-xs-3"><g:submitButton name="cancel" value="cancel"
                                                                   onclick='resourcenamechange1(${it.id})'/></span>

                        </div>
                        <span class="col-xs-12">${it.createdBy.userName} <span>@${it.createdBy.firstName}</span></span>
                        <span class="col-xs-12">
                            <g:if test="${it instanceof com.linksharing.resources.LinkResource}">${it.description}</g:if>
                            <br>
                            <g:if test="${it instanceof com.linksharing.resources.DocumentResource}">${it.description}</g:if>
                        </span>


                        <br>
                        <br>
                        <br>

                        <div class="col-xs-12">
                            <span><img src="/images/images1.png" alt="Smiley face" height="10" width="10"></span>
                            <span><img src="/images/images2.png" alt="Smiley1 face" height="10" width="10">
                            </span>
                            <span><img src="/images/images3.png" alt="Smiley3 face" height="10" width="10">
                            </span>
                            <g:if
                                    test="${it instanceof com.linksharing.resources.DocumentResource}"><span>
                                <ls:canbyview topicid="${it.topic.id}" resourceid="${it.id}"/></span>
                            </g:if>
                            <g:if
                                    test="${it instanceof com.linksharing.resources.LinkResource}"><span><a
                                    href="${it.url}" target="_blank"
                                    style="font-size:10px;">view fullsite</a>
                            </span>
                            </g:if>

                            <g:if test="${it.createdBy.id == session.user.id}">
                                <span class="glyphicon glyphicon-file " onclick="updateingresource(${it.id})"></span>
                            </g:if>

                            <span><ls:checkRead resourceid="${it.id}"></ls:checkRead></span>
                            <span><a href="<g:createLink controller="resource" action="showresource"
                                                         params="[id: it.id]"></g:createLink>"
                                     style="font-size:10px;">view post</a></span>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>

    </div>
</div>

</body>
</html>