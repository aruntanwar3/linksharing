package com.linksharing

import grails.validation.Validateable

/**
 * Created by arun on 2/3/16.
 */

@Validateable
class PersonCO {
    String personname
    String personid
    int page

    static constraint={

    }
}
