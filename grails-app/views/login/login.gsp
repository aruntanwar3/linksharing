<%@ page import="com.linksharing.*" %>


<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>

<div class="container" id="showdata">

    <div class="col-xs-6">
        <div class="panel panel-default" style="border:1px solid black">

            <div class="panel panel-heading" style="margin:0px; border:1px solid black ">

                <span>Recent Share</span>
            </div>

            <g:render template="/resource/recentShare"></g:render>

        </div>


        <ls:toppost/>

    </div>


    <div class="col-xs-6">
        <div class="panel panel-default" style="border:1px solid black;">
            <div class="panel panel-heading" style="border:1px solid black;">Login</div>

            <div class="panel panel-body" style="-webkit-box-shadow: none">

                <g:render template="../user/login"></g:render>


            </div>


            <div class="panel panel-default" style="border:1px solid black;">
                <div class="panel panel-heading" style="border:1px solid black;">Register</div>

                <div class="panel panel-body" style="-webkit-box-shadow: none">
                    <g:render template="../user/register"></g:render>
                </div>

            </div>

        </div>

    </div>

</div>
</body>
</html>