<%@ page import="com.linksharing.Topic" %>
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><span>Share link(popup)</span></h4>
        </div>

        <div class="modal-body">

            <g:form controller="resource" action="save" >

                <label for="link" class="col-xs-3">link*:</label>
                <input type="text" class="form-control col-xs-7 " id="link" placeholder="link" style="width:400px" name="url">
                <br>
                <br>
                <label for="description" class="col-xs-3">Description:</label>
                <g:textArea name="desc"  rows="5" cols="40"></g:textArea>
                <br>
                <br>
                <label for="topic" class="col-xs-3">Topics:</label>

                <div class="dropdown col-xs-7 " style="width:400px">

                    <g:select name="topic" from="${Topic.list()}" optionValue="name" value="Grails" optionKey="id"></g:select>

                </div>
                <br>
<br>
                <div>
                    <g:actionSubmit value="Save"/>

                </div>

            </g:form>
        </div>
    </div>
</div>
