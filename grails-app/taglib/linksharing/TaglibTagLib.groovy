package linksharing

import com.linksharing.Subscription
import com.linksharing.Topic
import com.linksharing.User
import com.linksharing.resources.ReadingItem
import com.linksharing.resources.Resource
import com.linksharing.resources.ResourceRating

class TaglibTagLib {
    //static defaultEncodeAs = [taglib: 'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "ls"


    def checkRead = { attrs ->
        if (session.user) {
            Resource resource = Resource.findById(attrs.resourceid)
            ReadingItem readingItem = ReadingItem.findByResourceAndUser(resource, session.user)
            if (readingItem?.isRead) {
                out << g.link(action: "checkReads", controller: "ReadingItem", params: [resourceid: attrs.resourceid, user: attrs.sessionuser, isRead: 'false'], "Mark As UnRead")
            } else {
                out << g.link(action: "checkReads", controller: "ReadingItem", params: [resourceid: attrs.resourceid, user: attrs.sessionuser, isRead: 'true'], "Mark As Read")

            }
        }
    }


    def trendingtopic = {

        out << g.render(template: '/topic/trendingtopic')
    }

    def toppost = {
        out << g.render(template: '/resource/toppost')

    }

    def canDeleteResource = { attrs ->

        Long resourceid = Long.valueOf(attrs.resourceid)
        Resource resource = Resource.findById(resourceid)
        println "under candeleteresource +++${resource.createdBy.class}++++++++++${session.user.class}"

        out << g.link(action: "delete", controller: "Resource", params: [id: attrs.resourceid], "Delete")
    }

    def checkSubscription = { attrs ->


        Long topicid = Long.valueOf(attrs.topicid)
        Topic topic = Topic.findById(topicid)
        Subscription subscription = Subscription.findByTopicAndUser(topic, session.user)
        if (subscription) {
            out << "<a href='javascript: void 0' class='unsubscribe' id='" + subscription.id + "'>unsubscribe</a>"

        } else {
            out << "<a href='javascript: void 0' class='subscribe' id='" + topicid + "'>subscribe</a>"
        }

    }


    def subscriptioncount = { attrs ->
        Long topicid = Long.valueOf(attrs.topicid)
        Topic topic = Topic.findById(topicid)
        def result = Subscription.createCriteria().list {
            projections {
                count('id')
            }
            eq("topic", topic)
        }
        out << result[0]


    }

    def userImage = { attrs ->
        def imageSrc = "/user/showPayload/${attrs.userId}"
        out << "<img src=\"${imageSrc}\" alt=\"Smiley face\"\n" +
                "                 style=\"border:1px solid black\" height=\"64px\" width=\"64px\"></a>"
    }


    def canbyview = { attrs ->

        int count = 0
        Topic topic = Topic.get(attrs.topicid)
        User user = new User()
        List<User> userList = user.getSubscribedUser(topic)
        userList.each {
            println("****************${session.user}")
            if ((session.user && it.id == session.user?.id) || session.user?.admin) {
                count = 1
            }
        }
        if (count == 1) {
            out << " <a href=\"/documentResource/download/${attrs.resourceid}\" style=\"font-size:10px\">Download</a>"
        }

    }
    def canbyviewlink = { attrs ->
println "sdds"
        int count = 0
        Topic topic = Topic.get(attrs.topicid)
        println "________${topic}"
        User user = new User()
        List<User> userList = user.getSubscribedUser(topic)
        println "****${userList}"
        userList.each {
            println("****************${session.user}")
            if ((session.user && it.id == session.user?.id) || session.user?.admin) {
                count = 1
            }
        }
        if (count == 1) {
            out << " <a href=\"${attrs.resourceid.url}\" target=\"_blank\"\n" +
                    "                                 style=\"font-size:10px;\">view fullsite</a>"
        }

    }


    def update = { attr, body ->
        Topic topic = Topic.get(attr.topic)
        if ((topic.subscribedUser.contains(session.user)) || (session.user.admin)) {
            out << " <a href=\"#\" class=\"glyphicon glyphicon-envelope\" style=\"padding:0px 7px;margin:0px 7px\"\n" +
                    "                               data-toggle=\"modal\" data-target=\"#myModal2\"></a>\n" +
                    "                            <g:render template=\"/topic/email\"></g:render>"
            if ((topic.createdBy == session.user) || (session.user.admin)) {
                out << " <a  data-toggle=\"modal\" data-target=\"#myModal3\" class=\"glyphicon glyphicon-pencil\" style=\"padding:0px 7px;margin:0px 7px\"></a>"
                out << render(template: '/topic/topicedit', model: [topic: topic])
            }
        }
    }

    def seriousness = { attr, body ->
        Topic topic = Topic.get(attr.topic)
        User user = User.get(session.user.id)
        if ((topic.subscribedUser.contains(session.user)) || (session.user.admin)) {
            Subscription subscription = user.getSubscription(attr.topic)
            if ((topic.subscribedUser.contains(session.user)) || (session.user.admin)) {
                out << "<select class=\"form-control\" name=\"seriousness\" id=\"sub${subscription.id}\" onclick=seriousnesschange(${subscription.id})>\n" +
                        "                        <option id=\"CASUAL\"value=\"casual\">Casual</option>\n" +
                        "                        <option id=\"SERIOUS\" value=\"serious\">Serious</option>\n" +
                        "                        <option id=\"VERY_SERIOUS\"value=\"very serious\">Very Serious</option>\n" +
                        "                    </select>"
            }
        }

    }

}

