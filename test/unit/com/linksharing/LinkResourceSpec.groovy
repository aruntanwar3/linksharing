package com.linksharing

import com.linksharing.resources.LinkResource
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(LinkResource)
class LinkResourceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test for linkResource"() {
        given :
        LinkResource linkResource=new LinkResource(url: "www.grails.com")

        expect:
        linkResource.toString()=="www.grails.com"
    }
}
