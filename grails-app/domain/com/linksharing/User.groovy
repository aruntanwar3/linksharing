package com.linksharing

import com.linksharing.resources.ReadingItem
import com.linksharing.resources.Resource
import com.linksharing.resources.ResourceRating

import java.sql.Blob


class User {

    String email
    String userName
    String firstName
    String lastName;
    Blob photo
    Boolean admin;
    Boolean activeField
    String password
    String confirmPassword
    Date dateCreated
    Date lastUpdated

    static transients = ['fullName', 'confirmPassword', 'SubscribedTopic']

    String getfullName() {
        return firstName + " " + lastName
    }

    static hasMany = [topics: Topic, subscriptions: Subscription, readingItems: ReadingItem, resources: Resource]

    static constraints = {

        userName unique: true, nullable: true, blank: false
        firstName nullable: false, blank: false
        lastName nullable: false, blank: false
        email email: true, unique: true, nullable: false, blank: false
        password nullable: false, blank: false, size: 5..15
        admin nullable: true
        photo nullable: true
        activeField nullable: true
        confirmPassword bindable: true, nullable: true, blank: true
//        confirmPassword bindable:true ,nullable: false,blank: true, validator: {val,obj->
//            println(val +"    "+obj.password)
//            obj.password == val?true:['invalid.matchingpassword']
//        }


    }

    static mapping = {
//        sort id: 'desc'
        topics lazy: false
        subscriptions lazy: false


    }
    static namedQueries = {
        search {
            UserSearchCO co ->
                if (co.active == null) {
                    if (co.firstName) {
                        eq("firstName", co.firstName)
                    }
                    if (co.lastName) {
                        eq("lastName", co.lastName)
                    }
                    if (co.email) {
                        eq("email", co.email)
                    }
                } else if (co.active) {
                    eq('active', co.active)
                    if (co.firstName) {
                        eq("firstName", co.firstName)
                    }
                    if (co.lastName) {
                        eq("lastName", co.lastName)
                    }
                    if (co.email) {
                        eq("email", co.email)
                    }
                } else {
                    eq('active', co.active)
                    if (co.firstName) {
                        eq("firstName", co.firstName)
                    }
                    if (co.lastName) {
                        eq("lastName", co.lastName)
                    }
                    if (co.email) {
                        eq("email", co.email)
                    }
                }
        }
    }

    List<Resource> findPublicResources() {
        List<Resource> resourceList = Resource.createCriteria().list(max: 5) {

            createAlias('topic', 'topicalias')
            eq('topicalias.visiblity', Visiblity.PUBLIC)
            order('dateCreated', 'desc')
        }
        return resourceList;
    }


    List<Subscription> getSubscribedTopic(User userr) {
        List<Subscription> subscriptionList = Subscription.createCriteria().list(max: 5) {
            eq('user', userr)
        }

        return subscriptionList
    }


    List<Resource> resourceList() {

        List<Resource> resourceList = Resource.createCriteria().list(max: 5) {

            createAlias('readingItems', 'ri')
            eq('ri.isRead', false)
            eq('ri.user', this)
        }
        /* List<Resource> resourceList=findPublicResources.find{
             it.readingItems.any{!it.isRead}
         }
         println "RESOURCE LIST::::${resourceList}"*/
        return resourceList

    }

    List<User> getSubscribedUser(Topic topic) {
        List<User> subscriptionUser = Subscription.createCriteria().list {
            projections {
                property('user')
            }
            eq('topic', topic)
        }

        return subscriptionUser
    }


    List<Resource> toppost() {
        List result = ResourceRating.createCriteria().list(max: 5) {
            createAlias('resource', 'res')
            projections {
                groupProperty('resource')
                avg('score')
                property('res.id')

            }
            order('score', 'desc')
        }
        List<Resource> resourceListt = []
        result.eachWithIndex { val, inx ->
            Resource resource = Resource.get(val.getAt(2))
            resourceListt.add(resource)
        }

        return resourceListt
    }


    int getscore(Long resourceid) {
        Resource resource = Resource.get(resourceid)
        if (resource) {
            ResourceRating resourceRating = ResourceRating.findByResourceAndUser(resource, this)
            if (!resourceRating) {
                return 0
            }
            return resourceRating?.score
        } else {
            return 0
        }

    }

    List<Topic> searchres(String s, Integer offset, Integer max) {

        List<Resource> resourceList1 = Resource.createCriteria().list(offset: offset, max: max) {
            println "<<<<<<<<<<<"

            like("description", "%${s}%")


        }
        if (!resourceList1) {
            List<Resource> resourceList = Resource.createCriteria().list(offset: offset, max: max) {
                createAlias('topic', 'top')
                like("top.name", "%${s}%")


            }
            return resourceList
        }



        return resourceList1

    }

    Integer sizeOf(String s){

        List<Resource> resourceList1= Resource.createCriteria().list(){
            println"<<<<<<<<<<<"

            like("description","%${s}%")


        }
        if (!resourceList1)
        {
            List<Resource> resourceList = Resource.createCriteria().list(){
                createAlias('topic','top')
                like("top.name", "%${s}%")



            }
            return resourceList.size()
        }



        return resourceList1.size()

        /*List<Resource> resource(String s,Integer offset,Integer max) {
            List<Resource> resourceList = Resource.createCriteria().list(offset:offset,max:max){
                like('description', "%${s}%")
            }
            return resourceList

        }*/

}
}