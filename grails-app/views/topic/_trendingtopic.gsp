%{--<% println topicVOList%>--}%
<g:each in="${topicVOList}">
    <div class="panel panel-body" style="-webkit-box-shadow: none">

        <div class="col-xs-4">
            <a href="<g:createLink controller="topic" action="index" params="[id:it.topic.createdBy.id]"></g:createLink>"><img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.createdBy.id}"
                 alt="Smiley face"
                 style="border:1px solid black" height="80px" width="80px"></a>


        </div>

        <div class="col-xs-8">

            <div id="topic-${it.topic.id}" style="display: none;">
                <span class="col-xs-3">
                <input type="text" id="topicn-${it.topic.id}"   class="topicName" size="4" placeholder="${it.name}"/>
        </span>
            <span class="col-xs-3"><g:submitButton name="save" value="save" onclick='topicnamechange(${it.topic.id})'/></span>
            <span class="col-xs-3"><g:submitButton name="cancel" value="cancel" onclick='topicnamechange1(${it.topic.id})'/></span>

        </div>

        <span class="col-xs-12">
            <a href="<g:createLink controller="topic" action="show" params="[id: it.topic.id]"></g:createLink>">${it.name}</a>
        </span>
        <span class="col-xs-5">@${it.createdBy.firstName}</span>
        <span class="col-xs-5">subscribe</span>
        <span class="col-xs-2">Post</span>
        <span class="col-xs-5"><g:if test="${session.user}"><ls:checkSubscription topicid="${it.topic.id}"/></g:if></span>
        <span class="col-xs-5">${it.topic.subscriptions.size()}</span>
        <span class="col-xs-2">${it.topic.resources.size()}</span>
    </div>

        <div class="col-xs-12" style="margin:0px; ">


%{--<g:if test="${session.user}">
            <g:if test="${it.topic.createdBy.id ==session.user.id }">

                <div class="col-xs-4">
                    <div class="dropdown">
                        <g:select id="seriousnesses" class="seriousnesses" name="seriousnesses"
                                  from="${com.linksharing.Seriousness.values()}"
                                  keys="${com.linksharing.Seriousness.values()*.name()}"
                                  value="${it.topic.subscriptions.user.subscriptions.seriousness}" onchange="changeSeriousness(${it.id})"/>
                        --}%%{--<g:select name="seriousnesses" id="seriousnesses" from="${seriousnesses}" value="${it.seriousness}" onchange="changeSeriousness(${it.id})"></g:select>--}%%{--

                    </div>
                </div>
            </g:if>
</g:if>--}%





            <g:if test="${session.user}">
                <g:if test="${it.topic.createdBy.id == session.user.id}">
                    <div class="col-xs-4">
                        <div class="dropdown">

                            <g:select id="visiblities" class="visblity" name="visiblities"
                                      from="${com.linksharing.Visiblity.values()}"
                                      keys="${com.linksharing.Visiblity.values()*.name()}"
                                      value="${it.topic.visiblity}" onchange="changeVisiblity(${it.topic.id})"/>

                        </div>

                    </div>

                    <div class="col-xs-4">
                        <span class="glyphicon glyphicon-envelope" ></span>
                        <span class="glyphicon glyphicon-file "onclick="updateingtopic(${it.topic.id})"></span>
                        <span class="glyphicon glyphicon-trash "onclick="deletetopic(${it.topic.id})"></span></div>

                </g:if>
            </g:if>

        </div>

    </div>
</g:each>

