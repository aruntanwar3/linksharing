package com.linksharing

import com.linksharing.resources.Resource

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


class SubscriptionController {

    def userService

    static allowedMethods = [save: "GET", update: "PUT" ]


    def index() {
        List<Subscription> subscriptionList=Subscription.createCriteria().list(max:5){
        }
        def count=Subscription.count()
        render(view: "/subscription/subscriptionlist", model: ['subscriptionList':subscriptionList,'count':count])

    }

    def paginatesubscription()
    {
        List<Subscription> subscriptionList=Subscription.createCriteria().list (max:params.max,offset:params.offset){
        }
        def count=Subscription.count()
        render(view: "/subscription/subscriptionlist", model: ['subscriptionList':subscriptionList,'count':count])
    }

    def delete(Long id) {
        println "under delete subscription*******************${id}"
        Subscription subscription = Subscription.findById(id)
        if (subscription) {
            subscription.delete(flush:true)
            flash.message="unsubscribed"

        }
        render "success"

    }


    def save(Long id) {
        println "under save subscription"
        Topic topic = Topic.findById(id)
        Subscription subscription = new Subscription(seriousness: Seriousness.CASUAL, topic: topic, user: session.user)
        println("subscription is there +++++++++${subscription}++++++${topic} +++${session.user}")
        if (!subscription.validate()) {
            subscription.errors.each {
                println it
            }

        }
        else {
            Subscription.withNewTransaction {
                subscription.save(flush:true)
                flash.message="subscribed user"
            }


        }
    render "success"
    }


    def updateTo() {
        long  id=Long.parseLong(params.id)
        Seriousness seriousness=Seriousness.valueOF(params.seriousness)
        Subscription subscription = Subscription.findById(id)
        if ( subscription){
            subscription.seriousness=seriousness
            subscription.save(flush:true)
        }
        render "sucesss"





    }



}
