package com.linksharing

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.junit.Ignore
import spock.lang.IgnoreRest
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Topic)
@Mock([User,Topic])
class TopicSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @IgnoreRest
    void "canary"(){
        given:
        1==1

        when:
        1==1

        then:
        assert true
    }


    void "test for topic"() {

        given:
        Topic topic=new Topic(visiblity: visi, topics: tops, createdBy: creator)

        expect:
        topic.validate()==result

        where:

        visi|tops|creator|result
        Visiblity.PUBLIC|"grails"|new User()|true

    }


    void "Checking Uniqueness"()
    {

        setup:
        User user=new User()
        Topic topic=new Topic(topicName:"history",createdBy:user,visibility:Visiblity.PUBLIC)
        when:
        topic.save()

        then:
        topic.count() == 1

        when:
        Topic topic2=new Topic(topicName:"history",createdBy:user,visibility:Visiblity.PUBLIC)
        topic2.save()

        then:
        topic2.count() == 1
        topic2.errors.allErrors.size() == 1
        topic2.errors.getFieldErrorCount('createdBy') == 1
    }
}
