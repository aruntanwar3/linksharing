package com.linksharing

/**
 * Created by arun on 25/2/16.
 */
/* gorm2 1functionality*/class RatingInfoVO {
    Integer totalScore
    Integer totalVote
    Integer averageScore

    @Override
    String toString()
    {
        return "Totalscore is :${totalScore},TotalVote is: ${totalVote},AverageScore is :${averageScore}"
    }
}
