package com.linksharing

import grails.test.mixin.TestFor
import spock.lang.IgnoreRest
import spock.lang.Specification



@TestFor(User)
class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }



    void "test user"() {
        given:
        User user=new User(firstName: fname,lastName: lname, email: emaill, photo:phot,password: passwd, admin:admin1)

        expect:
        user.validate()==result



        where:
        fname|lname|emaill|phot|passwd|admin1|result
        "arun"|"tanwar"|"arun.tanwar@tothenew.com"|null|"arun1234"|null|true
        " "|"tanwar"|"arun.tanwar@tothenew.com"|null|"arun1234"|null|false
    }


  void "Unique Email"()
{

    setup:
    String testEmail = "arun@tothenew.com"
    User user=new User(email:testEmail,firstName:"testFirstName",lastName:"testLastName",password: "123456" )

    when:
    user.save()

    then:
    user.count() == 1

    when:
    User user2=new User(email:testEmail,firstName:"testFirstName",lastName:"testLastName",password: "123456" )
    user2.save()

    then:
    user2.count() == 1
    user2.errors.allErrors.size() == 1
    user2.errors.getFieldErrorCount('email') == 1

}


    void "test for tranisent"()
    {
        given:
        User normalUser = new User(email: 'normal@tothenew.com', password: "normal1234", firstName: "normal", admin: false, userName: "aruntanwar", lastName: "user", confirmPassword: "normal1234")

        expect:
        normalUser.getfullName()=="normal user"
    }


}
