package com.linksharing

class Subscription {
    Seriousness seriousness
    User user
    Topic topic
    Date dateCreated
    Date lastUpdated
    static belongsTo = [topic: Topic]

    static constraints = {
        seriousness nullable: false
        user nullable: false
        topic nullable: false, unique: ['user']
    }
}
