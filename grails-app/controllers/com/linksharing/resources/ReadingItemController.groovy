package com.linksharing.resources

import com.linksharing.User
import grails.converters.JSON


class ReadingItemController {

    def checkReads(boolean isRead,Long resourceid) {

        User user=session.user
        Resource resource=Resource.get(resourceid)
        ReadingItem.executeUpdate("update ReadingItem set isRead=true where resource=? and user=?",[resource,user])
        forward(controller: "user",action: "index",params: ['user':user,'resource':resource])
        flash.message="message is read"
        render([0:"message save sucessfully",1:"errror while updating is read"] as JSON)

    }

}
