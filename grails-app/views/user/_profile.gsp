<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="col-xs-5">
    <g:render template="/user/show" model="['user':user1]"/>


    <div class="panel panel-default " style="border:1px solid black ">
        <div class=" panel panel-heading" style=" border:1px solid black;">
            <div class="row"><span class="col-xs-12">Topics</span>
            </div>
        </div>

        <g:each in="${topics}">
            <div class="panel panel-body" style="-webkit-box-shadow: none">
                <div class="colxs-12">
                    <div class="col-xs-5">
                        <span class="col-xs-12"><a href="<g:createLink controller="topic" action="show"
                                                                       params="[id: it.id]"></g:createLink>">${it.name}</a>
                        </span>
                    </div>

                    <div class="col-xs-4">
                        <span class="col-xs-12">Subscriptions</span>
                    </div>

                    <div class="col-xs-3">
                        <span class="col-xs-12">Posts</span>
                    </div>

                </div>

                <div class="colxs-12">
                    <div class="col-xs-5">
                        <div class="col-xs-6">
                            <div class="dropdown">
                                <g:select name="seriousnesss" from="${seriousnesses}" style="width: 80px" value="it"></g:select>

                            </div>
                        </div>
                        <span class="col-xs-5"><span class="glyphicon glyphicon-envelope "></span></span>

                    </div>

                    <div class="col-xs-4">
                        <span class="col-xs-12">${it.subscriptions.size()}</span>
                    </div>

                    <div class="col-xs-3">
                        <span class="col-xs-12">${it.resources.size()}</span>
                    </div>

                </div>

            </div>
        </g:each>

    </div>


    <div class="panel panel-default " style="border:1px solid black ">
        <div class=" panel panel-heading" style=" border:1px solid black;">
            <div class="row"><span class="col-xs-12">Topics</span>
            </div>
        </div>

        <g:each in="${topicList}">
            <div class="panel panel-body" style="-webkit-box-shadow: none">
                <div class="colxs-12">
                    <div class="col-xs-5">
                        <span class="col-xs-12"><a href="<g:createLink controller="topic" action="show"
                                                                       params="[id: it.id]"></g:createLink>">${it.name}</a>
                        </span>
                    </div>

                    <div class="col-xs-4">
                        <span class="col-xs-12">Subscriptions</span>
                    </div>

                    <div class="col-xs-3">
                        <span class="col-xs-12">Posts</span>
                    </div>

                </div>

                <div class="colxs-12">
                    <div class="col-xs-5">
                        <div class="col-xs-7">
                            <div class="dropdown">
                                <g:select name="seriousnesss" from="${seriousnesses}" style="width: 80px"  value="it"></g:select>

                            </div>
                        </div>
                        <span class="col-xs-5"><span class="glyphicon glyphicon-envelope "></span></span>

                    </div>

                    <div class="col-xs-4">
                        <span class="col-xs-12">${it.subscriptions.size()}</span>
                    </div>

                    <div class="col-xs-3">
                        <span class="col-xs-12">${it.resources.size()}</span>
                    </div>

                </div>

            </div>
        </g:each>

    </div>
</div>
<div class="col-xs-6">
    <div class="panel panel-default" style="border:1px solid black">
        <div class="panel panel-heading" style="margin:0px; border:1px solid black ">
            <div>Post</div>
        </div>

        <g:each in="${resourceList}">

            <div class="panel panel-body" style="-webkit-box-shadow: none">

                <div class="col-xs-4">
                    <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.createdBy.id}" alt="Smiley face"
                         style="border:1px solid black" height="120px" width="150px">
                </div>


                <div class="col-xs-8">
                    <span class="col-xs-12">${it.createdBy.userName} <span>@${it.createdBy.firstName}</span></span>
                    <span class="col-xs-12"><g:if
                            test="${it instanceof com.linksharing.resources.LinkResource}">${it.url}</g:if><br><g:if
                            test="${it instanceof com.linksharing.resources.DocumentResource}">${it.filepath}</g:if>
                    </span>


                    <br>
                    <br>
                    <br>

                    <div class="col-xs-12">
                        <span><img src="/images/images1.png" alt="Smiley face" height="10" width="10"></span>
                        <span><img src="/images/images2.png" alt="Smiley1 face" height="10" width="10">
                        </span>
                        <span><img src="/images/images3.png" alt="Smiley3 face" height="10" width="10">
                        </span>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <g:if
                                test="${it instanceof com.linksharing.resources.DocumentResource}"><span><a
                                href="<g:createLink controller="documentResource" action="download"
                                                    params="[id: it.id]"></g:createLink>"
                                style="font-size:10px;">Download</a></span>
                        </g:if>
                        <g:if
                                test="${it instanceof com.linksharing.resources.LinkResource}"><span><a href="${it.url}" target="_blank"
                                                                                                        style="font-size:10px;">view fullsite</a>
                        </span>
                        </g:if>

                        <span><a href="<g:createLink controller="resource" action="showresource"
                                                     params="[id: it.id]"></g:createLink>"
                                 style="font-size:10px;">view post</a></span>
                    </div>
                </div>
            </div>
        </g:each>
    </div>
</div>

</body>
</html>