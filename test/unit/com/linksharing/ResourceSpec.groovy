package com.linksharing

import com.linksharing.resources.LinkResource
import com.linksharing.resources.Resource
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Resource)
class ResourceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test for resource"() {
        given:
        LinkResource resource=new LinkResource(createdBy: creator,description: descript,topics: topics)

        expect:
        resource.validate()==result

        where:
        creator|descript|topics|result
        new User()|"abcd"|new Topic()|true

    }
}
