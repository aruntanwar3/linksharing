package com.linksharing

import com.linksharing.resources.DocumentResource
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(DocumentResource)
class DocumentResourceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "to String test"() {
        given:
        DocumentResource documentResource=new DocumentResource(filepath: "/home/abc")

        expect:
        documentResource.toString()=="/home/abc"
    }
}
