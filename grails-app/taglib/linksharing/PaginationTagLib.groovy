package linksharing

import com.linksharing.User

class PaginationTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "pg"

    def showAdmin ={attrs ,body->

        Boolean isAdmin=Boolean.valueOf(attrs.isAdmin)
        if(isAdmin)
        {
            out << body()
        }
    }


    def showUserList={

        List<User> userList=[]
        for(int i=1; i<=10; i++)
        {

            userList.add(new User(firstName: "User_${i}",lastName: "lastname_${i}",id: i))
        }
        out<<render(template: '/user/userList',model:[userList:userList])
    }


}
