package com.linksharing


enum Seriousness {
    CASUAL,
    VERY_SERIOUS,
    SERIOUS

    static Seriousness valueOF(String name) {
        return values().find { it.name() == name.toUpperCase() }
    }

}