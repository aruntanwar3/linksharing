<%@ page import="com.linksharing.Topic" %>

<div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

                <h4 class="modal-title"><span>Share Document(popup)</span></h4>

            </div>
            <div class="modal-body" >
                <g:uploadForm  controller="documentResource" action="upload">

                    <label for="document" class="col-xs-3">Document*:</label>
                    <input type='file'  id="document" name='document'/>
                    <br>
                    <br>
                    <label for="description" class="col-xs-3">Description:</label>
                    <g:textArea name="description"  rows="5" cols="40"></g:textArea>
                    <br>
                    <br>
                    <label for="topic" class="col-xs-3">Topics:</label>

                    <div class="dropdown col-xs-7 " style="width:400px">

                        <g:select name="topic" from="${Topic.list()}" optionValue="name" value="it" optionKey="id" ></g:select>

                        %{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Topics--}%
                        %{--<span class="caret"></span></button>--}%
                        %{--<ul class="dropdown-menu">--}%
                        %{--</ul>--}%
                    </div>
                    <br>
                    <br>
                    <div>
                        <g:actionSubmit value="Save" controller="documentResource" action="upload"/>
                        <g:actionSubmit value="Cancel" controller="user" action="index"/>
                    </div>

                </g:uploadForm>
            </div>
        </div>
    </div>
