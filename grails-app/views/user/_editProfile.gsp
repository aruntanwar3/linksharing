<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="col-xs-6">
    <g:render template="/user/show"/>

    <div class="panel panel-default " style="border:1px solid black ">
        <div class=" panel panel-heading" style=" border:1px solid black;">
            <div class="row"><span class="col-xs-8">Topic</span>

            </div>
        </div>
        <g:each in="${topicList}">
            <div class="panel panel-body" style="-webkit-box-shadow: none">

                <div class="col-xs-4">
                    <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${it.createdBy.id}"
                         alt="Smiley face"
                         style="border:1px solid black" height="80px" width="80px">

                </div>

                <div class="col-xs-8">

                    <div id="topic-${it.id}" style="display: none;">
                        <span class="col-xs-3">
                            <input type="text" id="topicn-${it.id}" class="topicName" size="4"
                                   placeholder="${it.name}"/>
                        </span>
                        <span class="col-xs-3"><g:submitButton name="save" value="save"
                                                               onclick='topicnamechange(${it.id})'/></span>
                        <span class="col-xs-3"><g:submitButton name="cancel" value="cancel"/></span>

                    </div>

                    <span class="col-xs-12">
                        <a href="<g:createLink controller="topic" action="show"
                                               params="[id: it.id]"></g:createLink>">${it.name}</a>
                    </span>
                    <span class="col-xs-5">@${it.createdBy.firstName}</span>
                    <span class="col-xs-5">subscribe</span>
                    <span class="col-xs-2">Post</span>
                    <span class="col-xs-5"></span>
                    <span class="col-xs-5">${it.subscriptions.size()}</span>
                    <span class="col-xs-2">${it.resources.size()}</span>
                </div>

                <g:if test="${it.createdBy.id == session.user.id}">
                    <div class="col-xs-12" style="margin:0px; ">
                        <div class="col-xs-4">
                            <div class="dropdown">
                                <g:select name="seriousnesss" from="${seriousnesses}"></g:select>

                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="dropdown">

                                <g:select id="visiblities" class="visblity" name="visiblities"
                                          from="${com.linksharing.Visiblity.values()}"
                                          keys="${com.linksharing.Visiblity.values()*.name()}"
                                          value="${it.visiblity}" onchange="changeVisiblity(${it.id})"/>

                            </div>

                        </div>

                        <div class="col-xs-4">
                            <span class="glyphicon glyphicon-envelope"></span>
                            <span class="glyphicon glyphicon-file " onclick="updateingtopic(${it.id})"></span>
                            <span class="glyphicon glyphicon-trash " onclick="deletetopic(${it.id})"></span></div>

                    </div>
                </g:if>

            </div>
        </g:each>

    </div>
</div>

<div class="col-xs-5">

    <div class="panel panel-default" style="border:1px solid black;">
        <div class="panel panel-heading" style="border:1px solid black;">Update profile</div>

        <div class="panel panel-body" style="-webkit-box-shadow: none">

            <g:uploadForm class="register-form1" novalidate="novalidate" controller="user" action="updatee">

                <label class="control-label col-sm-5" style="text-align:left">firstname:</label>

                <div class="col-sm-7">
                    <input type="text" class="form-control" id="fName" name="fName">
                </div>

                <br>
                <br>
                <br>

                <label class="control-label col-sm-5" style="text-align:left" for="lName">lastname:</label>

                <div class="col-sm-7">
                    <input type="text" class="form-control" id="lName" name="lName">
                </div>
                <br>
                <br>
                <br>


                <label class="control-label col-sm-5" style="text-align:left" for="email">Email:</label>

                <div class="col-sm-7">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                </div>
                <br>
                <br>
                <br>

                <label class="control-label col-sm-5" style="text-align:left" for="userName">Username:</label>

                <div class="col-sm-7">
                    <input type="text" class="form-control" id="userName" name="userName">

                </div>
                <br>
                <br>
                <br>

                <div>
                    <label class="control-label col-sm-5 " style="text-align:left"
                           for="payload">Photo:</label>

                    <div class="col-sm-7">
                        <input type="file" id="payload" name="payload">
                    </div>
                </div>

                <br>
                <br>
                <br>


                <div class=" col-sm-12">
                    <g:submitButton name="Update"/>
                </div>

            </g:uploadForm>
        </div>

    </div>



    <div class="panel panel-default" style="border:1px solid black;">
        <div class="panel panel-heading" style="border:1px solid black;">change password</div>

    <div class="panel panel-body" style="-webkit-box-shadow: none">

<g:form controller="user" action="changePassword">



    <label class="col-sm-5" style="text-align:left">Validate Number*:</label>

    <div class="col-sm-7">
        <input type="text" name="Number" class="form-control  " id="Number" placeholder="Validate Number"
               style="width:200px">
    </div>



    <br>
    <br>
    <br>
    <label for="email" class="col-xs-5">Email*:</label>
    <div class="col-sm-7">
        <input type="text" name="email" class="form-control col-xs-7 " id="email1" placeholder="Email"
               style="width:200px">
    </div>



    <br>
    <br>
    <br>

    <label for="changePassword" class="col-xs-5">Change Password*:</label>
    <div class="col-sm-7">
    <input type="password" name="changePassword" class="form-control  " id="changePassword"
           placeholder="changePassword"
           style="width:200px">
    </div>
    <br>
    <br>
    <br>




    <g:submitButton name="Change Password" id="Check" controller="user" action="changePassword"/> <span></span>



</g:form>
    </div>
    </div>


</div>
</body>
</html>