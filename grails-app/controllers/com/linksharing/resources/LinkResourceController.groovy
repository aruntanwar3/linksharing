package com.linksharing.resources

import com.linksharing.Topic

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LinkResourceController {
    def index()
    {
        List<Topic> topicList=Topic.createCriteria().list(max:5) {

        }
        def count=Topic.count()
        render(view: "/linkResource/resourceList", model: ['topicList':topicList,'count':count])
    }

    def paginatetopic()
    {
        List<Topic> topicList=Topic.createCriteria().list(max:params.max,offset:params.offset) {

        }
        def count=Topic.count()
        render(view: "/linkResource/resourceList", model: ['topicList':topicList,'count':count])


    }


}
