<%@ page import="com.linksharing.*" %>


<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta name="layout" content="main">
</head>

<body>
<div class="container"  id="showdata">


    <div class=" col-xs-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <div>
                    <div class="col-xs-2">
                        <a href="<g:createLink controller="topic" action="index" params="[id:resource.createdBy.id]"></g:createLink>"> <img src="${grailsApplication.config.grails.serverURL}/user/showPayload/${resource.createdBy.id}" alt="Smiley face"
                                                                                                                                            style="border:1px solid black" height="80px" width="80px"></a>
                    </div>

                    <div class="col-xs-10">
                        <div class="col-xs-12">
                            <div id="topic-${resource?.id}" style="display: none;">
                                <span class="col-xs-3">
                                    <input type="text" id="topicn-${resource?.id}"   class="topicName" size="4" placeholder="${resource.description}"/>
                                </span>
                                <span class="col-xs-3"><g:submitButton name="save" value="save" onclick='resourcenamechange(${resource?.id})'/></span>
                                <span class="col-xs-3"><g:submitButton name="cancel" value="cancel" onclick='resourcenamechange1(${resource?.id})'/></span>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom:5px">
                            <div class="col-xs-6">
                                <span class="h5">${resource.createdBy.getfullName()}</span>
                            </div>

                            <div class="col-xs-3">
                            </div>

                            <div class="col-xs-3">
                                <a href="<g:createLink controller="topic" action="show"  params="[id:resource?.topic?.id]" ></g:createLink>" class="text-left">${resource?.topic?.name}</a>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom:10px">
                            <div class="col-xs-4">
                                <small class="text-muted">@${resource?.createdBy?.firstName}</small>
                            </div>

                            <div class="col-xs-2"></div>

                            <div class="col-xs-6 text-muted">${resource?.dateCreated}</div>
                        </div>

                        <div class="row" style="padding-bottom:15px">
                            <div class="col-xs-8"></div>

                            <div class="col-xs-4">
                                <g:if test="${session.user}">

                                    <select class="form-control"   id="sel1" onchange="savedata(this.value,${resource?.id})">
                                        <option  <g:if test="${score==0}">selected="selected"</g:if> value="0">select star</option>
                                        <option  <g:if test="${score==1}">selected="selected"</g:if> value="1">1 Star</option>
                                        <option <g:if test="${score==2}">selected="selected"</g:if> value="2">2 Star</option>
                                        <option <g:if test="${score==3}">selected="selected"</g:if> value="3">3 Star</option>
                                        <option <g:if test="${score==4}">selected="selected"</g:if>value="4">4 Star</option>
                                        <option <g:if test="${score==5}">selected="selected"</g:if>value="5">5 Star</option>
                                    </select>
                                </g:if>
                            %{--<g:actionSubmit value="vote" controller="resourceRating" action="resourceRatingSave"></g:actionSubmit>
                       --}% </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>

                    <p style="padding-bottom:5px">${resource?.description}</p>

                    <div class="col-xs-12">
                        <span class="col-xs-8">
                            <span><img src="/images/images1.png" alt="Smiley face" height="10" width="10">
                            </span>
                            <span><img src="/images/images2.png" alt="Smiley1 face" height="10"
                                       width="10"></span>
                            <span><img src="/images/images3.png" alt="Smiley3 face" height="10"
                                       width="10"></span>
                        </span>



                        <g:if test="${session.user && (session.user.id==resource?.createdBy?.id ||(session.user?.admin))}">
                            <span><ls:canDeleteResource resourceid="${resource.id}"/></span>


                        </g:if>
                        <g:if test="${session.user}">
                            <g:if test="${resource?.createdBy?.id== session.user.id}">
                                <span class="glyphicon glyphicon-file "onclick="updateingtopic(${resource?.id})"></span>
                            </g:if>
                        </g:if>

                        <g:if test="${resource instanceof com.linksharing.resources.DocumentResource}">
                            <span><ls:canbyview topicid="${resource.topic.id}" resourceid="${resource.id}"/></span>
                        </g:if>
                        <g:if test="${resource instanceof com.linksharing.resources.LinkResource}">

                            <span><ls:canbyviewlink topicid="${resource.topic.id}" resourceid="${resource}"/>
                            </span>
                        </g:if>


                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="col-xs-5" style="margin:15px">
        <div class="panel panel-default " style="border:1px solid black ">
            <div class=" panel panel-heading" style=" border:1px solid black;">
                <div class="row"><span class="col-xs-8">Trending Topic</span>

                </div>
            </div>
            <ls:trendingtopic/>

        </div>
    </div>
</div>
</body>
</html>
