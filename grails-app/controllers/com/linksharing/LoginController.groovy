package com.linksharing

import com.linksharing.resources.Resource

import javax.sql.rowset.serial.SerialBlob

class LoginController {



    def index() {
        if (session.user) {
            redirect(controller: "user", action: "index")
        } else
        {

            User user = new User();
            List<Resource> resourceList = user.findPublicResources()
            List<Resource> resourcess = user.toppost()
            Topic topic = new Topic()
            List<TopicVO> topicVOList = topic.getTrendingTopic()
            render(view: "/login/login", model: ['resourceList': resourceList, 'resourcess': resourcess,'topicVOList': topicVOList])
        }
    }

    def loginHandler() {
        User exist = User.findByUserNameAndPassword(params.userName, params.password)
        if (exist) {
            if (exist.activeField) {
                session.user = exist
                flash.message=message(code:'User.login.successfully' ,args: [exist.userName])
                println flash.message

            } else
            {
                flash.error = "your account is not active"

            }
        } else {
            flash.error = message(code:'User.password.not.found')


        }
        forward(controller: 'login' ,action: 'index')


    }

    def logout() {
        session.user=null
        session.invalidate()

        forward(action: 'index')

    }

    def register() {

       User user = new User(firstName: params.fName, lastName: params.lName, password:params.pwd, confirmPassword:params.passwd, email:params.email,userName: params.userName)
        def uploadedFile = params.payload
        byte[] myByte= uploadedFile.getBytes()
        java.sql.Blob blob=null;
        blob=new SerialBlob(myByte)
        user.photo=blob
        if (user.save(flush: true,failOnError: true))
        {
            redirect(controller: 'login', action: 'index')
            flash.message="     ${user.userName } is sucessfully registered !!!"
        }
    }

    def topicSearch(){
        params.max=params.max?:5
        params.offset=params.offset?:0
        User user=new User()
//        List<Resource> resourceList1=user.resource(params.search,params.offset,params.max)
        List<Resource> resourceList2=user.searchres(params.search,params.offset,params.max)
        List<Resource> resourcess = user.toppost()
        Topic topic = new Topic()
        List<TopicVO> topicVOList = topic.getTrendingTopic()
        render(template: '/user/search',model: ['resourceList2':resourceList2,'topicVOList':topicVOList,'resourcess':resourcess,searchText:params.search,total:user.sizeOf(params.search)])
    }

    def topicSearching(){
        println params.search
        params.max=params.max?:5
        params.offset=params.offset?:0
        User user=new User()
//        List<Resource> resourceList1=user.resource(params.search,params.offset,params.max)
        List<Resource> resourceList2=user.searchres(params.search,Integer.parseInt(params.offset),Integer.parseInt(params.max))
        List<Resource> resourcess = user.toppost()
        Topic topic = new Topic()
        List<TopicVO> topicVOList = topic.getTrendingTopic()
        render(template: '/user/searchtemp',model: ['resourceList2':resourceList2,'topicVOList':topicVOList,'resourcess':resourcess,searchText:params.search,total:user.sizeOf(params.search)])
    }
}

