package com.linksharing.resources

import com.linksharing.RatingInfoVO
import com.linksharing.ResourceSearchCO
import com.linksharing.Seriousness
import com.linksharing.Topic
import com.linksharing.TopicVO
import com.linksharing.User
import com.linksharing.Visiblity
import org.hibernate.ObjectNotFoundException
import grails.transaction.Transactional


class ResourceController {

    def index() {

    }

    def search(ResourceSearchCO co) {
        if (co.q) {
            co.visiblity = Visiblity.PUBLIC
            List<Resource> resources = Resource.search(co).list();
            render(resources)
        } else
            render("Q is not set")
    }


    def showNew() {
        LinkResource ln = new LinkResource()
        RatingInfoVO vo = ln.ratingInfoVO
        render "totalscore is:${vo.totalVote},totalvote is:${vo.totalVote},averagescore is:${vo.averageScore}"
    }


    def showresource(Long id) {

        Resource resource = Resource.get(id)
        List<TopicVO> topicVOList = Topic.getTrendingTopic()
        User user = session.user
        int score
        if(session.user)
        {
             score=user.getscore(id)
        }
        else
        {
            score=0
        }
        render(view: "/resource/viewPost", model: ['resource': resource, 'topicVOList': topicVOList,'user':user,'score':score])
    }


    def delete(Long id) {
        Resource resource = Resource.findById(id)

        if (resource) {
            try {
                resource.delete(flush: true)
                println "resource deleted"
                redirect(controller: 'user', action: 'index')
                flash.message = "resource deleted"
            } catch (ObjectNotFoundException e) {
                render "${e.message}"
            }

        } else {
            render "resource not found"
        }

        forward(controller: "resource" ,action: "showresource")
        

    }

    @Transactional(readOnly = false)
    def save(String url, String desc, String topic) {
        Long id = Long.valueOf(topic).longValue();
        Topic topicn = Topic.findById(id)
        LinkResource linkResource = new LinkResource(url: url, description: desc, topic: topicn, createdBy: session.user)

        if (linkResource.save(flush: true)) {
            flash.message="resource successfully"
        }
        else {
            flash.error = "fill proper form"
        }
        redirect(controller: 'user', action: 'index')
    }

    def update(Long id,String desc)
    {
        Resource resource=Resource.findById(id)
        resource.description=desc
        resource.save(flush: true)
    }

    def updateresourcename()
    {
        String id = params.id
        def resourceId = Long.parseLong(id.trim())
        Resource resource=Resource.findById(resourceId)
        Topic.executeUpdate("UPDATE Resource set description=? where id=?",[params.resource,resourceId])
        render "success"
    }


}
