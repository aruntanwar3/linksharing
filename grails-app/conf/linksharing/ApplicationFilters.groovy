package linksharing

class ApplicationFilters {


    def filters = {
        all(controller: '*', action: '*') {
            before = {
            }
            after = { Map model ->
            }
            afterView = { Exception e ->
            }
        }

        sessioncheck(controller: '*', action: '*')
                {
                    before = {
                    }
                    after = { Map model ->
                    }
                    afterView = { Exception e ->
                    }
                }
        logincheck(controller: 'login', action: '*', invert: true)
                {
                    before = {
                        log.info("this is logging param" + params)


                        if (!session.user&& !(controllerName=='user'&& actionName=='showPayload')  && !(controllerName=='user'&& actionName=='showEmail')&& !(controllerName=='user'&& actionName=='showUserr')&&  !(controllerName=='user'&& actionName=='showUser')&& !(controllerName=='documentResource'&& actionName=='download')&& !(controllerName=='resource'&& actionName=='showresource')&&  !(controllerName == 'console')&& !(controllerName=='topic'&& actionName=='toppost')&& !(controllerName=='topic'&& actionName=='save')&& !(controllerName=='topic'&& actionName=='show')
                                &&!(controllerName=='linkresource'&&actionName=='save')&&!(controllerName=='topic'&&actionName=='index')
                                &&!(controllerName== 'resourceRating'&&actionName=='save')
                                &&!(controllerName== 'resource'&&actionName=='delete')&&!(controllerName== 'readinItem'&&actionName=='save')) {
                            redirect(controller: 'login', action: 'index')
                        }
                    }


                    after = { Map model ->
                    }
                    afterView = { Exception e ->
                    }
                }

    }

}
