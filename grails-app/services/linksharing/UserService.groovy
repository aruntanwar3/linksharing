package linksharing

import com.linksharing.Subscription
import com.linksharing.Topic
import com.linksharing.TopicVO
import com.linksharing.User
import com.linksharing.resources.Resource
import grails.transaction.Transactional

@Transactional
class UserService {

    def mailService

    def serviceMethod() {
        Topic topic = new Topic()
        List<TopicVO> topicVOList = topic.getTrendingTopic()
        topicVOList


    }

    def  sendTestMail(String email,String sub,String emailbody) {
        mailService.sendMail {
            to email
            subject sub
            body emailbody

        }
    }

    def getSubscriptionSize(User user){
        def criteria=Subscription.createCriteria()
        def results=criteria.list {
            projections {
                count("id")
            }
            eq("user",user)
        }
        return results
    }


    def getPostCount(User user)
    {
        def results=Resource.createCriteria().list {
            projections
                    {
                        count("id")
                    }
            eq("createdBy",user)
        }
        return results

    }
}
