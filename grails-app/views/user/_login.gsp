<div class="col-xs-12">
<form class="form-horizontal" role="form" novalidate="novalidate">

    <div class="form-group">
        <label class="control-label col-sm-5" style="text-align:left" for="userName">Username:</label>

        <div class="col-sm-7">
            <input type="text" class="form-control" id="userName" name="userName">

        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-5 " style="text-align:left" for="pwd">Password:</label>

        <div class="col-sm-7">
            <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password">
        </div>
    </div>


    <div class="form-group">
        <div class=" col-sm-12">
            <g:actionSubmit value="Login" controller="login" action="loginHandler"/>
        </div>
    </div>
    </form>

    <div class="col-xs-12">
    <a href="#myModal5" style="text-align:left" class="col-xs-6" data-toggle="modal">forget password</a>
        <a href="#myModal6" style="text-align:left" class="col-xs-6" data-toggle="modal">change password</a>

    </div>

    <div  id="myModal5"  class="modal fade"  role="dialog">
         <g:render template="../user/forgetPassword"></g:render>
    </div>



</div>






