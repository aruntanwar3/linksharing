<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <title>Subscribed Users</title>
</head>

<body>
<div class="container">
    <div class="col-xs-6">
        <div class="panel panel-default " style="border:1px solid black ">
            <div class=" panel panel-heading" style=" border:1px solid black;">
                <div class="row"><span class="col-xs-8">Trending Topic</span>

                </div>
            </div>
            <ls:trendingtopic/>

        </div>

        <ls:toppost/>
    </div>

    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <strong>Search for "test"</strong></div>
                </div>
            </div>

            <div class="panel-body">
                <div>
                    <div class="col-xs-2">
                        <div class="glyphicon glyphicon-user" style="font-size:60px"></div>
                    </div>

                    <div class="col-xs-10">
                        <div class="row">
                            <div class="col-xs-6">
                                <span class="h5">Arun Tanwar</span>
                                <small class="text-muted">@arun</small>
                            </div>

                            <div class="col-xs-3">
                            </div>

                            <div class="col-xs-3">
                                <a href="#" class="text-left">Grails</a>
                            </div>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi risus, mollis sit amet purus vitae, rutrum commodo erat.</p>

                        <div class="row">
                            <div class="col-xs-1 fa fa-facebook-official"></div>

                            <div class="col-xs-1 fa fa-twitter"></div>

                            <div class="col-xs-1 fa fa-google-plus"></div>

                            <div class="col-xs-2"><small><a href="#">Download</a></small></div>

                            <div class="col-xs-3"><small><a href="#">View Full Site</a></small></div>

                            <div class="col-xs-2"><small><a href="#">Mark as Read</a></small></div>

                            <div class="col-xs-2"><small><a href="#">View Post</a></small></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>